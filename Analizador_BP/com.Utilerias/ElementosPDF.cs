﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Analizador_BP.com.Utilerias
{
    public class ElementosPDF
    {
        public class ElemPDF
        {
            private int id_Proceso;
            public int Id_Proceso
            {
                get { return id_Proceso; }
                set { id_Proceso = value; }
            }

            private int id_Usuario;
            public int Id_Usuario
            {
                get { return id_Usuario; }
                set { id_Usuario = value; }
            }

            private int id_TipoAnalizador;
            public int Id_TipoAnalizador
            {
                get { return id_TipoAnalizador; }
                set { id_TipoAnalizador = value; }
            }

            private int id_AplicacionP;
            public int Id_AplicacionP
            {
                get { return id_AplicacionP; }
                set { id_AplicacionP = value; }
            }

            private int id_EstatusP;
            public int Id_EstatusP
            {
                get { return id_EstatusP; }
                set { id_EstatusP = value; }
            }

            private DateTime fecha_CreacionP;
            public DateTime Fecha_CreacionP
            {
                get { return fecha_CreacionP; }
                set { fecha_CreacionP = value; }
            }

            private int id_Aplicacion;
            public int Id_Aplicacion
            {
                get { return id_Aplicacion; }
                set { id_Aplicacion = value; }
            }

            private string nombreCorto;
            public string NombreCorto
            {
                get { return nombreCorto; }
                set { nombreCorto = value; }
            }

            private string nombre;
            public string Nombre
            {
                get { return nombre; }
                set { nombre = value; }
            }

            private string ruta;
            public string Ruta
            {
                get { return ruta; }
                set { ruta = value; }
            }

            private int id_Tecnologia;
            public int Id_Tecnologia
            {
                get { return id_Tecnologia; }
                set { id_Tecnologia = value; }
            }

            private int framework;
            public int Framework
            {
                get { return framework; }
                set { framework = value; }
            }
            
            private int id_TipoAplicacion;
            public int Id_TipoAplicacion
            {
                get { return id_TipoAplicacion; }
                set { id_TipoAplicacion = value; }
            }

            private int versionLenguaje;
            public int VersionLenguaje
            {
                get { return versionLenguaje; }
                set { versionLenguaje = value; }
            }

            private int id_Estatus;
            public int Id_Estatus
            {
                get { return id_Estatus; }
                set { id_Estatus = value; }
            }

            private DateTime fecha_Creacion;
            public DateTime Fecha_Creacion
            {
                get { return fecha_Creacion; }
                set { fecha_Creacion = value; }
            }
            private string nombre_Tecnologia;
            public string Nombre_Tecnologia
            {
                get { return nombre_Tecnologia; }
                set { nombre_Tecnologia = value; }
            }
            private string nombre_TipoApp;
            public string Nombre_TipoApp
            {
                get { return nombre_TipoApp; }
                set { nombre_TipoApp = value; }
            }


            
            public ElemPDF(int Id_Proceso, int Id_Usuario, int Id_TipoAnalizador, int Id_AplicacionP, int Id_EstatusP, DateTime Fecha_CreacionP, int Id_Aplicacion, string NombreCorto, string Nombre, string Ruta, int Id_Tecnologia, int Framework, int Id_TipoAplicacion, int VersionLenguaje, int Id_Estatus, DateTime Fecha_Creacion, string Nombre_Tecnologia, string Nombre_TipoApp)
            {
                id_Proceso = Id_Proceso;
                id_Usuario = Id_Usuario;
                id_TipoAnalizador = Id_TipoAnalizador;
                id_AplicacionP = Id_AplicacionP;
                id_EstatusP = Id_EstatusP;
                fecha_CreacionP = Fecha_CreacionP;
                id_Aplicacion = Id_Aplicacion;
                nombreCorto = NombreCorto;
                nombre = Nombre;
                ruta = Ruta;
                id_Tecnologia = Id_Tecnologia;
                framework = Framework;
                id_TipoAplicacion = Id_TipoAplicacion;
                versionLenguaje = VersionLenguaje;
                id_Estatus = Id_Estatus;
                fecha_Creacion = Fecha_Creacion;
                nombre_Tecnologia = Nombre_Tecnologia;
                nombre_TipoApp=Nombre_TipoApp;


            }
        }

    }
}