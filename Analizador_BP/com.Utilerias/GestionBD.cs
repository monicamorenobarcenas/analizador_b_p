﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//using HerramientaAD.com.LogFisico;

namespace Analizador_BP.Utility
{
    public class GestionBD
    {
        private SqlConnection CadenaDeConexion;
        private SqlCommand Comando;
        private SqlTransaction TransaccionSQL;

        public GestionBD()
        {
            CadenaDeConexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionSQL"].ToString());
            Console.WriteLine("Crea Conexion");
        }
        public void CerrarConexion()
        {
            try
            {
                if (CadenaDeConexion != null)
                {
                    if (CadenaDeConexion.State != ConnectionState.Closed)
                        CadenaDeConexion.Close();
                }
            }
            catch (SqlException Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString() + " "
                                                              + Err.Number.ToString() + " "
                                                              + Err.Procedure.ToString() + " "
                                                              + Err.LineNumber.ToString() + " "
                                                              + Err.State.ToString());
            }
        }

        // --- Transacciones SQL ---
        public bool InicializaTransaccion()
        {
            bool respuesta = false;
            try
            {
                TransaccionSQL = CadenaDeConexion.BeginTransaction("TransaccionSQL");
                Console.WriteLine("Inicia Transaction");
            }
            catch (Exception Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool CommitTransaccion()
        {
            bool respuesta = false;
            try
            {
                TransaccionSQL.Commit();
            }
            catch (Exception Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool RollBackTransaccion()
        {
            bool respuesta = false;
            try
            {
                TransaccionSQL.Rollback();
            }
            catch (Exception Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool AsignaTransaccion()
        {
            bool respuesta = false;
            try
            {
                Comando.Transaction = TransaccionSQL;
            }
            catch (Exception Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString());
            }
            return respuesta;
        }
        // --- Procedimientos Almacenados ---
        public void PreparaStoredProcedure(string SP)
        {
            try
            {
                CadenaDeConexion.Open();
                Comando = new SqlCommand();
                Comando.Connection = CadenaDeConexion;
                Comando.CommandText = SP;
                Comando.CommandType = CommandType.StoredProcedure;
            }
            catch (SqlException Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString() + " "
                                                             + Err.Number.ToString() + " "
                                                             + Err.Procedure.ToString() + " "
                                                             + Err.LineNumber.ToString() + " "
                                                             + Err.State.ToString());
                CerrarConexion();

            }
        }
        public void EjecutaStoredProcedureNonQuery()
        {
            try
            {
                Comando.ExecuteNonQuery();
            }
            catch (SqlException Err)
            {
                CerrarConexion();
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString() + " "
                                                             + Err.Number.ToString() + " "
                                                             + Err.Procedure.ToString() + " "
                                                             + Err.LineNumber.ToString() + " "
                                                             + Err.State.ToString());
            }
        }

        public SqlDataReader AlmacenarStoredProcedureDataReader()
        {
            SqlDataReader sqlDataReader = null;
            try
            {
                sqlDataReader = Comando.ExecuteReader();
            }
            catch (SqlException Err)
            {
                CerrarConexion();
                Console.WriteLine("BaseDeDatos.AlmacenarStoredProcedureDataReader " + Err.Message.ToString() + " "
                                                                               + Err.Number.ToString() + " "
                                                                               + Err.Procedure.ToString() + " "
                                                                               + Err.LineNumber.ToString() + " "
                                                                               + Err.State.ToString());
            }
            return sqlDataReader;
        }

        public void CargaParametro(string Parametro, SqlDbType Tipo, int Tamano, ParameterDirection Direccion, object Valor)
        {
            try
            {
                Comando.Parameters.Add(new SqlParameter(Parametro, Tipo, Tamano, Direccion, true, 0, 0, "", DataRowVersion.Current, Valor));
            }
            catch (SqlException Err)
            {
                CerrarConexion();
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString() + " "
                                                             + Err.Number.ToString() + " "
                                                             + Err.Procedure.ToString() + " "
                                                             + Err.LineNumber.ToString() + " "
                                                             + Err.State.ToString());
            }

        }

        public object RegresaValorParam(string name)
        {
            try
            {
                return Comando.Parameters[name].Value;
            }
            catch (Exception Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString());
                return 0;
            }
        }
    }
}