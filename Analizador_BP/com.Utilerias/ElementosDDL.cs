﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Analizador_BP.com.Utilerias
{
    public class ElementosDDL
    {
        
        public class EleDDL
        {
         
            private int id_DLL;
            public int Id_DLL
            {
                get { return id_DLL; }
                set { id_DLL = value; }
            }

            private string nombre;
            public string Nombre
            {
                get { return nombre; }
                set { nombre = value; }
            }

            private string ruta;
            public string Ruta
            {
                get { return ruta; }
                set { ruta = value; }
            }

            private int id_Tecnologia;
            public int Id_Tecnologia
            {
                get { return id_Tecnologia; }
                set { id_Tecnologia = value; }
            }
            private string nomTecnologia;
            public string NomTecnologia
            {
                get { return nomTecnologia; }
                set { nomTecnologia = value; }
            }

            private string version;
            public string Version
            {
                get { return version; }
                set { version = value; }
            }

            private int id_Estatus;
            public int Id_Estatus
            {
                get { return id_Estatus; }
                set { id_Estatus = value; }
            }
            private DateTime fecha_Registro;
            public DateTime Fecha_Registro
            {
                get { return fecha_Registro; }
                set { fecha_Registro = value; }
            }

            
          private string nomEstatus;
            public string NomEstatus
            {
                get { return nomEstatus; }
                set { nomEstatus = value; }
            }
            public EleDDL(int Id_DLL, string Nombre, string Ruta, int Id_Tecnologia, string NomTecnologia, string Version, int Id_Estatus, DateTime Fecha_Registro, string NomEstatus)
            {
                id_DLL = Id_DLL;
                nombre = Nombre;
                ruta = Ruta;
                id_Tecnologia = Id_Tecnologia;
                nomTecnologia = NomTecnologia;
                version = Version;
                id_Estatus = Id_Estatus;
                fecha_Registro = Fecha_Registro;
                nomEstatus = NomEstatus;
            }
        }

        public class ElemenDDL
        {

            private int id_DLL;
            public int Id_DLL
            {
                get { return id_DLL; }
                set { id_DLL = value; }
            }

            private string nombre;
            public string Nombre
            {
                get { return nombre; }
                set { nombre = value; }
            }
            private string nombre_Ejecucion;
            public string Nombre_Ejecucion
            {
                get { return nombre_Ejecucion; }
                set { nombre_Ejecucion = value; }
            }

            private string ruta;
            public string Ruta
            {
                get { return ruta; }
                set { ruta = value; }
            }

            private int id_Tecnologia;
            public int Id_Tecnologia
            {
                get { return id_Tecnologia; }
                set { id_Tecnologia = value; }
            }         
            private string version;
            public string Version
            {
                get { return version; }
                set { version = value; }
            }

            private int id_Estatus;
            public int Id_Estatus
            {
                get { return id_Estatus; }
                set { id_Estatus = value; }
            }
            private DateTime fecha_Registro;
            public DateTime Fecha_Registro
            {
                get { return fecha_Registro; }
                set { fecha_Registro = value; }
            }
            private string nombreTecnologia;
            public string NombreTecnologia
            {
                get { return nombreTecnologia; }
                set { nombreTecnologia = value; }
            }

            public ElemenDDL(int Id_DLL, string Nombre, string Nombre_Ejecucion, string Ruta, int Id_Tecnologia,  string Version, int Id_Estatus, DateTime Fecha_Registro, string NombreTecnologia)
            {
                id_DLL = Id_DLL;
                nombre = Nombre;
                nombre_Ejecucion = Nombre_Ejecucion;
                ruta = Ruta;
                id_Tecnologia = Id_Tecnologia;
                version = Version;
                id_Estatus = Id_Estatus;
                fecha_Registro = Fecha_Registro;
                nombreTecnologia = NombreTecnologia;
            }
        }
        public class EleDLLEliAgr
        {
            private int resultado;
            public int Resultado
            {
                get { return resultado; }
                set { resultado = value; }
            }
            public EleDLLEliAgr(int Resultado)
            {
                resultado = Resultado;
            }
        }
    }
}