﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Analizador_BP.com.Utilerias
{
    //Aplicacion="Relación de Compras" TipoAplicacion="Escritorio" URL="" Tecnologia="C Sharp" Framework=" v4.0" 

    public class ElementosExcel
    {
        private string aplicacion;
        public string Aplicacion
        {
            get { return aplicacion; }
            set { aplicacion = value; }
        }

        private string claveSonarQube;
        public string ClaveSonarQube
        {
            get { return claveSonarQube; }
            set { claveSonarQube = value; }
        }

        private string tipoAplicacion;
        public string TipoAplicacion
        {
            get { return tipoAplicacion; }
            set { tipoAplicacion = value; }
        }

        private string nombreCarpeta;
        public string NombreCarpeta
        {
            get { return nombreCarpeta; }
            set { nombreCarpeta = value; }
        }

        private string url;
        public string URL
        {
            get { return url; }
            set { url = value; }
        }

        private string tecnologia;
        public string Tecnologia
        {
            get { return tecnologia; }
            set { tecnologia = value; }
        }

        private string framework;
        public string Framework
        {
            get { return framework; }
            set { framework = value; }
        }
        //Lenguaje=" Visual Studio 2012" Ambiente=" Desarrollo, Pruebas, Producción" IPServidor="192.168.112.142" S0="Windows Server" 

        private string lenguaje;
        public string Lenguaje
        {
            get { return lenguaje; }
            set { lenguaje = value; }
        }
        private string ambiente;
        public string Ambiente
        {
            get { return ambiente; }
            set { ambiente = value; }
        }
        private string iPServidor;
        public string IPServidor
        {
            get { return iPServidor; }
            set { iPServidor = value; }
        }

        private string s0;
        public string S0
        {
            get { return s0; }
            set { s0 = value; }
        }

        //Version_SistemaOperativo="2003" Hostname="LSTKEG101033" workstation="" Middleware="" VersionadorCodigo="" IPRepositorio="" 

        private string version_SistemaOperativo;
        public string Version_SistemaOperativo
        {
            get { return version_SistemaOperativo; }
            set { version_SistemaOperativo = value; }
        }

        private string hostname;
        public string Hostname
        {
            get { return hostname; }
            set { hostname = value; }
        }

        private string workstation;
        public string Workstation
        {
            get { return workstation; }
            set { workstation = value; }
        }

        private string middleware;
        public string Middleware
        {
            get { return middleware; }
            set { middleware = value; }
        }

        private string versionadorCodigo;
        public string VersionadorCodigo
        {
            get { return versionadorCodigo; }
            set { versionadorCodigo = value; }
        }

        private string iPRepositorio;
        public string IPRepositorio
        {
            get { return iPRepositorio; }
            set { iPRepositorio = value; }
        }
        //CodigoFuente="SI" Documentacion="" ArchivoConexion="" InterfazEntrada="244.0.0.1" InterfazSalida="244.0.0.1" ComponentePrincipal="" 

        private string codigoFuente;
        public string CodigoFuente
        {
            get { return codigoFuente; }
            set { codigoFuente = value; }
        }
        private string documentacion;
        public string Documentacion
        {
            get { return documentacion; }
            set { documentacion = value; }
        }
        private string archivoConexion;
        public string ArchivoConexion
        {
            get { return archivoConexion; }
            set { archivoConexion = value; }
        }
        private string interfazEntrada;
        public string InterfazEntrada
        {
            get { return interfazEntrada; }
            set { interfazEntrada = value; }
        }

        private string interfazSalida;
        public string InterfazSalida
        {
            get { return interfazSalida; }
            set { interfazSalida = value; }
        }
        private string componentePrincipal;
        public string ComponentePrincipal
        {
            get { return componentePrincipal; }
            set { componentePrincipal = value; }
        }
        //Dependencias=" AccesoDatos.dll, Enumeradores.dll, firefox.exe, HXW.dll, log4net.dll, Npgsql.dll, ProbarConexion.exe, PuenteConsumirServicioREST.exe" 
        //BasedeDatos="" IpBasedeDatos="" HostnameBasedeDatos="" Horario="" Pais="" EstatusAplicacion="" EstatusControl="" NombreEntrevistadoNegocio="" 

        private string dependencias;
        public string Dependencias
        {
            get { return dependencias; }
            set { dependencias = value; }
        }
        private string basedeDatos;
        public string BasedeDatos
        {
            get { return basedeDatos; }
            set { basedeDatos = value; }

        }

        private string ipBasedeDatos;
        public string IpBasedeDatos
        {
            get { return ipBasedeDatos; }
            set { ipBasedeDatos = value; }
        }
        private string hostnameBasedeDatos;
        public string HostnameBasedeDatos
        {
            get { return hostnameBasedeDatos; }
            set { hostnameBasedeDatos = value; }
        }
        private string horario;
        public string Horario
        {
            get { return horario; }
            set { horario = value; }
        }
        private string pais;
        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }
        private string estatusAplicacion;
        public string EstatusAplicacion
        {
            get { return estatusAplicacion; }
            set { estatusAplicacion = value; }
        }
        private string estatusControl;
        public string EstatusControl
        {
            get { return estatusControl; }
            set { estatusControl = value; }
        }
        private string nombreEntrevistadoNegocio;
        public string NombreEntrevistadoNegocio
        {
            get { return nombreEntrevistadoNegocio; }
            set { nombreEntrevistadoNegocio = value; }
        }
        //Gerencia="" Area="" SubArea="" PuestoEntrevistadoNegocio="" ExpertoNegocio="" InsumosEntrada="SI" CualesInsumosEntrada=" bandera.txt, ControlWeb.xml, DLLActivo.txt, LogActivacionTelcel.txt, LogCancelacion.txt, LogCapturarAbono.txt, LogConcursoRuleta.txt, LogConfirmacionPlanSemanal.txt, LogConfirmaCT.txt, LogConsulta.txt, LogConsultaFonacot.txt, LogCT.txt, LogD100.txt, LogD300.txt, LogDevolucion.txt, LogElectronica.txt, LogErrorCSharp.txt, LogFactura.txt, LogInicioDiaMuebles.txt, LogMenuPolizaUdi.txt, LogMinijunta.txt, LogMovimientosPagosTarjetas.txt, LogProcesoMotocicleta.txt, LogProGC.txt, LogRecargaTARegalo.txt, LogReconstruccion.txt, LogReembolso.txt, LogTransferencia.txt, LogTransferenciasApartado.txt, LogTraspazoNuevoAUdi.txt"
        private string gerencia;
        public string Gerencia
        {
            get { return gerencia; }
            set { gerencia = value; }
        }
        private string area;
        public string Area
        {
            get { return area; }
            set { area = value; }
        }
        private string subArea;
        public string SubArea
        {
            get { return subArea; }
            set { subArea = value; }
        }
        private string puestoEntrevistadoNegocio;
        public string PuestoEntrevistadoNegocio
        {
            get { return puestoEntrevistadoNegocio; }
            set { puestoEntrevistadoNegocio = value; }
        }
        private string expertoNegocio;
        public string ExpertoNegocio
        {
            get { return expertoNegocio; }
            set { expertoNegocio = value; }
        }
        private string insumosEntrada;
        public string InsumosEntrada
        {
            get { return insumosEntrada; }
            set { insumosEntrada = value; }
        }
        private string cualesInsumosEntrada;
        public string CualesInsumosEntrada
        {
            get { return cualesInsumosEntrada; }
            set { cualesInsumosEntrada = value; }
        }
        // InsumosSalida="SI" CualesInsumosSalida=" bandera.txt, ControlWeb.xml, DLLActivo.txt, LogActivacionTelcel.txt, LogCancelacion.txt, LogCapturarAbono.txt, LogConcursoRuleta.txt, LogConfirmacionPlanSemanal.txt, LogConfirmaCT.txt, LogConsulta.txt, LogConsultaFonacot.txt, LogCT.txt, LogD100.txt, LogD300.txt, LogDevolucion.txt, LogElectronica.txt, LogErrorCSharp.txt, LogFactura.txt, LogInicioDiaMuebles.txt, LogMenuPolizaUdi.txt, LogMinijunta.txt, LogMovimientosPagosTarjetas.txt, LogProcesoMotocicleta.txt, LogProGC.txt, LogRecargaTARegalo.txt, LogReconstruccion.txt, LogReembolso.txt, LogTransferencia.txt, LogTransferenciasApartado.txt, LogTraspazoNuevoAUdi.txt" 
        //ProcesoNormativo=" " ManualUsuario="" DocumentoRequerimiento="" ManualInstalacion="" DocumentoSoporte="" DiseñoTecnico="" 

        private string insumosSalida;
        public string InsumosSalida
        {
            get { return insumosSalida; }
            set { insumosSalida = value; }
        }

        private string cualesInsumosSalida;
        public string CualesInsumosSalida
        {
            get { return cualesInsumosSalida; }
            set { cualesInsumosSalida = value; }
        }
        private string procesoNormativo;
        public string ProcesoNormativo
        {
            get { return procesoNormativo; }
            set { procesoNormativo = value; }
        }
        private string manualUsuario;
        public string ManualUsuario
        {
            get { return manualUsuario; }
            set { manualUsuario = value; }
        }
        private string documentoRequerimiento;
        public string DocumentoRequerimiento
        {
            get { return documentoRequerimiento; }
            set { documentoRequerimiento = value; }
        }
        private string manualInstalacion;
        public string ManualInstalacion
        {
            get { return manualInstalacion; }
            set { manualInstalacion = value; }
        }
        private string documentoSoporte;
        public string DocumentoSoporte
        {
            get { return documentoSoporte; }
            set { documentoSoporte = value; }
        }
        private string diseñoTecnico;
        public string DiseñoTecnico
        {
            get { return diseñoTecnico; }
            set { diseñoTecnico = value; }
        }
        //DiseñoFuncional="" MatrizCasosUso="" CasosPrueba="" ErroresConocidos="" NombreEntrevistadoTecnico="" ExpertoAplicacionIT="" 
        //SoftwareTerceros="" FrameworksNegocio="" 
        private string diseñoFuncional;
        public string DiseñoFuncional
        {
            get { return diseñoFuncional; }
            set { diseñoFuncional = value; }
        }
        private string matrizCasosUso;
        public string MatrizCasosUso
        {
            get { return matrizCasosUso; }
            set { matrizCasosUso = value; }
        }
        private string casosPrueba;
        public string CasosPrueba
        {
            get { return casosPrueba; }
            set { casosPrueba = value; }
        }
        private string erroresConocidos;
        public string ErroresConocidos
        {
            get { return erroresConocidos; }
            set { erroresConocidos = value; }
        }
        private string nombreEntrevistadoTecnico;
        public string NombreEntrevistadoTecnico
        {
            get { return nombreEntrevistadoTecnico; }
            set { nombreEntrevistadoTecnico = value; }
        }
        private string expertoAplicacionIT;
        public string ExpertoAplicacionIT
        {
            get { return expertoAplicacionIT; }
            set { expertoAplicacionIT = value; }
        }
        private string softwareTerceros;
        public string SoftwareTerceros
        {
            get { return softwareTerceros; }
            set { softwareTerceros = value; }
        }
        private string frameworksNegocio;
        public string FrameworksNegocio
        {
            get { return frameworksNegocio; }
            set { frameworksNegocio = value; }
        }
        //VersionFrameworkNegocio="" MotorBasedeDatos=" Oracle, SQL Server" VersionMotorBasedeDatos="" EsquemaInstanciaBasedeDatos="" ProcesosBatch="" 
        private string versionFrameworkNegocio;
        public string VersionFrameworkNegocio
        {
            get { return versionFrameworkNegocio; }
            set { versionFrameworkNegocio = value; }
        }
        private string motorBasedeDatos;
        public string MotorBasedeDatos
        {
            get { return motorBasedeDatos; }
            set { motorBasedeDatos = value; }
        }
        private string versionMotorBasedeDatos;
        public string VersionMotorBasedeDatos
        {
            get { return versionMotorBasedeDatos; }
            set { versionMotorBasedeDatos = value; }
        }
        private string esquemaInstanciaBasedeDatos;
        public string EsquemaInstanciaBasedeDatos
        {
            get { return esquemaInstanciaBasedeDatos; }
            set { esquemaInstanciaBasedeDatos = value; }
        }
        private string procesosBatch;
        public string ProcesosBatch
        {
            get { return procesosBatch; }
            set { procesosBatch = value; }
        }
        //CualesProcesosBatch="" AutomaticoManual="" HorarioEjecucionBatch="" ServidorDesarrollo="SI" IpServidorDesarrollo="192.168.112.50" 
        //HostNameServidorDesarrollo="LSTKEG101029" ServidorQA="SI" IpServidorQA="192.168.112.89" HostnameServidorQA="LSTKEG101031"
        private string cualesProcesosBatch;
        public string CualesProcesosBatch
        {
            get { return cualesProcesosBatch; }
            set { cualesProcesosBatch = value; }
        }
        private string automaticoManual;
        public string AutomaticoManual
        {
            get { return automaticoManual; }
            set { automaticoManual = value; }
        }
        private string horarioEjecucionBatch;
        public string HorarioEjecucionBatch
        {
            get { return horarioEjecucionBatch; }
            set { horarioEjecucionBatch = value; }
        }
        private string servidorDesarrollo;
        public string ServidorDesarrollo
        {
            get { return servidorDesarrollo; }
            set { servidorDesarrollo = value; }
        }
        private string ipServidorDesarrollo;
        public string IpServidorDesarrollo
        {
            get { return ipServidorDesarrollo; }
            set { ipServidorDesarrollo = value; }
        }
        private string hostNameServidorDesarrollo;
        public string HostNameServidorDesarrollo
        {
            get { return hostNameServidorDesarrollo; }
            set { hostNameServidorDesarrollo = value; }
        }
        private string servidorQA;
        public string ServidorQA
        {
            get { return servidorQA; }
            set { servidorQA = value; }
        }
        private string ipServidorQA;
        public string IpServidorQA
        {
            get { return ipServidorQA; }
            set { ipServidorQA = value; }
        }
        private string hostnameServidorQA;
        public string HostnameServidorQA
        {
            get { return hostnameServidorQA; }
            set { hostnameServidorQA = value; }
        }

        

        public ElementosExcel(string Aplicacion, string ClaveSonarQube, string TipoAplicacion,string NombreCarpeta, string URL, string Tecnologia, string Framework, string Lenguaje
, string Ambiente, string IPServidor, string S0, string Version_SistemaOperativo, string Hostname, string Workstation
, string Middleware, string VersionadorCodigo, string IPRepositorio, string CodigoFuente, string Documentacion, string ArchivoConexion
, string InterfazEntrada, string InterfazSalida, string ComponentePrincipal, string Dependencias, string BasedeDatos, string IpBasedeDatos, string HostnameBasedeDatos, string Horario, string Pais
, string EstatusAplicacion, string EstatusControl, string NombreEntrevistadoNegocio, string Gerencia, string Area, string SubArea
, string PuestoEntrevistadoNegocio, string ExpertoNegocio, string InsumosEntrada, string CualesInsumosEntrada, string InsumosSalida, string CualesInsumosSalida, string ProcesoNormativo, string ManualUsuario, string DocumentoRequerimiento, string ManualInstalacion
, string DocumentoSoporte, string DiseñoTecnico, string DiseñoFuncional, string MatrizCasosUso, string CasosPrueba
, string ErroresConocidos, string NombreEntrevistadoTecnico, string ExpertoAplicacionIT, string SoftwareTerceros, string FrameworksNegocio
, string VersionFrameworkNegocio, string MotorBasedeDatos, string VersionMotorBasedeDatos, string EsquemaInstanciaBasedeDatos
, string ProcesosBatch, string CualesProcesosBatch, string AutomaticoManual, string HorarioEjecucionBatch, string ServidorDesarrollo
, string IpServidorDesarrollo, string HostNameServidorDesarrollo, string ServidorQA, string IpServidorQA
, string HostnameServidorQA)
        {
            aplicacion = Aplicacion;
            claveSonarQube = ClaveSonarQube;
            tipoAplicacion = TipoAplicacion;
            nombreCarpeta = NombreCarpeta;
            url = URL;
            tecnologia = Tecnologia;
            framework = Framework;
            lenguaje = Lenguaje;
            ambiente = Ambiente;
            iPServidor = IPServidor;
            s0 = S0;
            version_SistemaOperativo = Version_SistemaOperativo;
            hostname = Hostname;
            workstation = Workstation;
            middleware = Middleware;
            versionadorCodigo = VersionadorCodigo;
            iPRepositorio = IPRepositorio;
            codigoFuente = CodigoFuente;
            documentacion = Documentacion;
            archivoConexion = ArchivoConexion;
            interfazEntrada = InterfazEntrada;
            interfazSalida = InterfazSalida;
            componentePrincipal = ComponentePrincipal;
            dependencias = Dependencias;
            basedeDatos = BasedeDatos;
            ipBasedeDatos = IpBasedeDatos;
            hostnameBasedeDatos = HostnameBasedeDatos;
            horario = Horario;
            pais = Pais;
            estatusAplicacion = EstatusAplicacion;
            estatusControl = EstatusControl;
            nombreEntrevistadoNegocio = NombreEntrevistadoNegocio;
            gerencia = Gerencia;
            area = Area;
            subArea = SubArea;
            puestoEntrevistadoNegocio = PuestoEntrevistadoNegocio;
            expertoNegocio = ExpertoNegocio;
            insumosEntrada = InsumosEntrada;
            cualesInsumosEntrada = CualesInsumosEntrada;
            insumosSalida = InsumosSalida;
            cualesInsumosSalida = CualesInsumosSalida;
            procesoNormativo = ProcesoNormativo;
            manualUsuario = ManualUsuario;
            documentoRequerimiento = DocumentoRequerimiento;
            manualInstalacion = ManualInstalacion;
            documentoSoporte = DocumentoSoporte;
            diseñoTecnico = DiseñoTecnico;
            diseñoFuncional = DiseñoFuncional;
            matrizCasosUso = MatrizCasosUso;
            casosPrueba = CasosPrueba;
            erroresConocidos = ErroresConocidos;
            nombreEntrevistadoTecnico = NombreEntrevistadoTecnico;
            expertoAplicacionIT = ExpertoAplicacionIT;
            softwareTerceros = SoftwareTerceros;
            frameworksNegocio = FrameworksNegocio;
            versionFrameworkNegocio = VersionFrameworkNegocio;
            motorBasedeDatos = MotorBasedeDatos;
            versionMotorBasedeDatos = VersionMotorBasedeDatos;
            esquemaInstanciaBasedeDatos = EsquemaInstanciaBasedeDatos;
            procesosBatch = ProcesosBatch;
            cualesProcesosBatch = CualesProcesosBatch;
            automaticoManual = AutomaticoManual;
            horarioEjecucionBatch = HorarioEjecucionBatch;
            servidorDesarrollo = ServidorDesarrollo;
            ipServidorDesarrollo = IpServidorDesarrollo;
            hostNameServidorDesarrollo = HostNameServidorDesarrollo;
            servidorQA = ServidorQA;
            ipServidorQA = IpServidorQA;
            hostnameServidorQA = HostnameServidorQA;
        }

       
    }
}
