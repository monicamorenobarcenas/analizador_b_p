﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Analizador_BP.Utility
{
    public class ListasDesplegablesAplicaciones
    {
        private int indice;
        public int Indice
        {
            get { return indice; }
            set { indice = value; }
        }

        private string texto;
        public string Texto
        {
            get { return texto; }
            set { texto = value; }
        }
        private int id_Tecnologia;
        public int Id_Tecnologia
        {
            get { return id_Tecnologia; }
            set { id_Tecnologia = value; }
        }
        public ListasDesplegablesAplicaciones(int Indice, string Texto, int Id_Tecnologia)
        {
            indice = Indice;
            texto = Texto;
            id_Tecnologia = Id_Tecnologia;
        }

        public ListasDesplegablesAplicaciones()
        {
        }
    }
}