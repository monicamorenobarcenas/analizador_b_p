﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Analizador_BP.Enums
{
    public enum Enumeraciones_Aplicacion
    {
        
        ListadoTecnologias = 2,
        ListadoTipoAplicacion = 3,
        AplicacionesConsulta = 5,
        AplicacionesAgregar = 6,
        AplicacionesEliminar = 7,
        AplicacionesModificar = 8,
    }
}