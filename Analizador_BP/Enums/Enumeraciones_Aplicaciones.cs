﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Analizador_BP.Enums
{
    public enum  Enumeraciones_Aplicaciones
    {
        Consulta = 1,
        ConsultaTecnologia = 2,
        ConsultaTipoAplicacion = 3,
        Actualiza = 4,
        AplicacionesConsulta = 5,
        ConsultaPorFecha =6,
        AplicacionesAgregar = 7,
        AplicacionesEliminar = 8,
        AplicacionesModificar = 9,
        ConsultaCriticidades = 10

        //ListadoTecnologias = 2,
        //ListadoTipoAplicacion = 3,
        //AplicacionesConsulta = 5,
        //AplicacionesAgregar = 6,
        //AplicacionesEliminar = 7,
        //AplicacionesModificar = 8,
    }
}