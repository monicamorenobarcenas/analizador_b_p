﻿using Analizador_BP.Datos;
using Analizador_BP.com.Utilerias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Analizador_BP.Utility;
using Analizador_BP.Enums;

namespace Analizador_BP.Models
{
    public class AnalizadorModel
    {
        private List<ListasDesplegablesAplicaciones> appLista = new List<ListasDesplegablesAplicaciones>();
        private List<ElementosDDL.ElemenDDL> listDll = new List<ElementosDDL.ElemenDDL>();
        private List<ElementosExcel> listExcel = new List<ElementosExcel>();

        public List<ElementosDDL.ElemenDDL> ListDll
        {
            get { return listDll; }
            set { listDll = value; }
        }
        public List<ElementosExcel> ListExcel
        {
            get { return listExcel; }
            set { listExcel = value; }
        }
        public List<ListasDesplegablesAplicaciones> AppLista
        {
            get { return appLista; }
            set { appLista = value; }
        }

        public DatosProcesos datos = new DatosProcesos();

        /**Atributos SP_PROCESOS**/
        private string app;
        private int idProceso;
        private int idUsuario;
        private string idTipoAnalizador;
        private int idEstatus;
        private string fechaAnalisis;

        /**Atributos SP_CATTECNOLOGIAS**/
        private string nombreTecnologia;
        private int idTecnologia;

        /**Atributos SP_CATANALIZADOR**/
        private string nombreAnalizador;
        private int idAnalizador;
        private bool isCheck;
        /**Atributos SP_CATTECNOLOGIAS**/
        private int id_Aplicacion;
        public int Id_Aplicacion
        {
            get { return id_Aplicacion; }
            set { id_Aplicacion = value; }
        }

        public string NombreTecnologia
        {
            get { return nombreTecnologia; }
            set { nombreTecnologia = value; }
        }
        public int IdTecnologia
        {
            get { return idTecnologia; }
            set { idTecnologia = value; }
        }
        public string NombreAnalizador
        {
            get { return nombreAnalizador; }
            set { nombreAnalizador = value; }
        }

        public int IdAnalizador
        {
            get { return idAnalizador; }
            set { idAnalizador = value; }
        }
        public bool IsCheck
        {
            get { return isCheck; }
            set { isCheck = value; }
        }
        private XmlDocument detalleXML;
        private XmlDocument detalleXMLProcesos;
        private XmlDocument detalleXMLAplicaciones;
        public XmlDocument DetalleXML
        {
            get { return detalleXML; }
            set { detalleXML = value; }
        }

        public XmlDocument DetalleXMLProcesos
        {
            get { return detalleXMLProcesos; }
            set { detalleXMLProcesos = value; }
        }
        public XmlDocument DetalleXMLAplicaciones
        { get { return  detalleXMLAplicaciones; }
            set { detalleXMLAplicaciones = value; }
        }
        public string Aplicacion
        {
            get { return app; }
            set { app = value; }
        }

        public int IdProceso
        {
            get { return idProceso; }
            set { idProceso = value; }
        }

        public int IdUsuario
        {
            get { return idUsuario; }
            set { idUsuario = value; }
        }

        public string IdTipoAnalizador
        {
            get { return idTipoAnalizador; }
            set { idTipoAnalizador = value; }
        }

        public int IdEstatus
        {
            get { return idEstatus; }
            set { idEstatus = value; }
        }

        public string Fecha
        {
            get { return fechaAnalisis; }
            set { fechaAnalisis = value; }
        }

        

        public AnalizadorModel()
        {

        }
        public AnalizadorModel(int IdProceso, int IdEstatus, string Aplicacion, string IdTipoAnalizador, string FechaAnalisis)

        {
            app = Aplicacion;
            idProceso = IdProceso;
            idTipoAnalizador = IdTipoAnalizador;
            idEstatus = IdEstatus;
            fechaAnalisis = FechaAnalisis;
        }

        public AnalizadorModel(int UsuarioID)
        {

            if (datos.ProcesosConsulta(UsuarioID))
            {
                detalleXMLProcesos = datos.ResultadoXML;
            }

            if (datos.AplicacionesConsulta((int)Enumeraciones_Aplicaciones.Consulta, UsuarioID))
            {
                XmlNode xmlNode = datos.ResultadoXML.DocumentElement.SelectSingleNode("Aplicaciones");
                foreach (XmlNode elemento in xmlNode.SelectNodes("row"))
                {
                   
                    appLista.Add(new ListasDesplegablesAplicaciones(
                        int.Parse(elemento.Attributes["Id_Aplicacion"].Value.ToString()),
                        elemento.Attributes["Nombre"].Value.ToString(),
                        int.Parse(elemento.Attributes["Id_Tecnologia"].Value.ToString())
                        ));
                }
                detalleXMLAplicaciones = datos.ResultadoXML;
            }

            if (datos.TipoAnalizadorConsulta(UsuarioID))
            {
                detalleXML = datos.ResultadoXML;
            }
        }

       
        public AnalizadorModel(int Tipo, int UsuarioID, int Id_Aplicacion)
        {
            if (datos.DLLsConsulta(Tipo, UsuarioID, Id_Aplicacion))
            {
                XmlNode xmlNode = datos.ResultadoXML.DocumentElement.SelectSingleNode("DDls");
                foreach (XmlNode elemento in xmlNode.SelectNodes("row"))
                {
                    listDll.Add(new ElementosDDL.ElemenDDL(
                        int.Parse(elemento.Attributes["Id_DLL"].Value.ToString()),
                        elemento.Attributes["Nombre"].Value.ToString(),
                        elemento.Attributes["Nombre_Ejecucion"].Value.ToString(),
                        elemento.Attributes["Ruta"].Value.ToString(),
                        int.Parse(elemento.Attributes["Id_Tecnologia"].Value.ToString()),
                        elemento.Attributes["Version"].Value.ToString(),
                        int.Parse(elemento.Attributes["Id_Estatus"].Value.ToString()),
                        DateTime.Parse(elemento.Attributes["Fecha_Registro"].Value.ToString()),
                        elemento.Attributes["NombreTecnologia"].Value.ToString()
                        ));
                }
            }
        }

        /// <summary>
        /// Consulta para excel
        /// </summary>
        /// <param name="Id_Aplicacion"></param>
        /// <param name="UsuarioID"></param>
        public AnalizadorModel(int Id_Aplicacion, string ids_Aplicaciones, int UsuarioID  )
        {
            try
            {
                if (datos.ConsultaExcel(Id_Aplicacion, ids_Aplicaciones))
                {
                    XmlNode xmlNode = datos.ResultadoXML.DocumentElement.SelectSingleNode("Inventario");
                    foreach (XmlNode elemento in xmlNode.SelectNodes("row"))
                    {
                        listExcel.Add(new ElementosExcel(
                           elemento.Attributes["Aplicacion"].Value != null || elemento.Attributes["Aplicacion"].Value.ToString() != "" ? elemento.Attributes["Aplicacion"].Value.ToString() : "",
                           elemento.Attributes["ClaveSonarQube"].Value != null || elemento.Attributes["ClaveSonarQube"].Value.ToString() != "" ? elemento.Attributes["ClaveSonarQube"].Value.ToString() : "",
                            elemento.Attributes["TipoAplicacion"].Value != null || elemento.Attributes["TipoAplicacion"].Value.ToString() != "" ? elemento.Attributes["TipoAplicacion"].Value.ToString() : "",
                            elemento.Attributes["NombreCarpeta"].Value != null || elemento.Attributes["NombreCarpeta"].Value.ToString() != "" ? elemento.Attributes["NombreCarpeta"].Value.ToString() : "",
                            elemento.Attributes["URL"].Value != null || elemento.Attributes["URL"].Value.ToString() != "" ? elemento.Attributes["URL"].Value.ToString() : "",
                            elemento.Attributes["Tecnologia"].Value != null || elemento.Attributes["Tecnologia"].Value.ToString() != "" ? elemento.Attributes["Tecnologia"].Value.ToString() : "",
                            elemento.Attributes["Framework"].Value != null || elemento.Attributes["Framework"].Value.ToString() != "" || elemento.Attributes["Framework"].Value.ToString() != " " ? elemento.Attributes["Framework"].Value.ToString() : "",
                            elemento.Attributes["Lenguaje"].Value != null || elemento.Attributes["Lenguaje"].Value.ToString() != "" ? elemento.Attributes["Lenguaje"].Value.ToString() : "",
                            elemento.Attributes["Ambiente"].Value != null || elemento.Attributes["Ambiente"].Value.ToString() != "" ? elemento.Attributes["Ambiente"].Value.ToString() : "",
                            elemento.Attributes["IPServidor"].Value != null || elemento.Attributes["IPServidor"].Value.ToString() != "" ? elemento.Attributes["IPServidor"].Value.ToString() : "",
                            elemento.Attributes["S0"].Value != null || elemento.Attributes["S0"].Value.ToString() != "" ? elemento.Attributes["S0"].Value.ToString() : "",
                            elemento.Attributes["Version_SistemaOperativo"].Value != null || elemento.Attributes["Version_SistemaOperativo"].Value.ToString() != "" ? elemento.Attributes["Version_SistemaOperativo"].Value.ToString() : "",
                            elemento.Attributes["Hostname"].Value != null || elemento.Attributes["Hostname"].Value.ToString() != "" ? elemento.Attributes["Hostname"].Value.ToString() : "",
                            elemento.Attributes["workstation"].Value != null || elemento.Attributes["workstation"].Value.ToString() != "" ? elemento.Attributes["workstation"].Value.ToString() : "",
                            elemento.Attributes["Middleware"].Value != null || elemento.Attributes["Middleware"].Value.ToString() != "" ? elemento.Attributes["Middleware"].Value.ToString() : "",
                            elemento.Attributes["VersionadorCodigo"].Value != null || elemento.Attributes["VersionadorCodigo"].Value.ToString() != "" ? elemento.Attributes["VersionadorCodigo"].Value.ToString() : "",
                            elemento.Attributes["IPRepositorio"].Value != null || elemento.Attributes["IPRepositorio"].Value.ToString() != "" ? elemento.Attributes["IPRepositorio"].Value.ToString() : "",
                            elemento.Attributes["CodigoFuente"].Value != null || elemento.Attributes["CodigoFuente"].Value.ToString() != "" ? elemento.Attributes["CodigoFuente"].Value.ToString() : "",
                            elemento.Attributes["Documentacion"].Value != null || elemento.Attributes["Documentacion"].Value.ToString() != "" ? elemento.Attributes["Documentacion"].Value.ToString() : "",
                            elemento.Attributes["ArchivoConexion"].Value != null || elemento.Attributes["ArchivoConexion"].Value.ToString() != "" ? elemento.Attributes["ArchivoConexion"].Value.ToString() : "",
                            elemento.Attributes["InterfazEntrada"].Value != null || elemento.Attributes["InterfazEntrada"].Value.ToString() != "" ? elemento.Attributes["InterfazEntrada"].Value.ToString() : "",
                            elemento.Attributes["InterfazSalida"].Value != null || elemento.Attributes["InterfazSalida"].Value.ToString() != "" ? elemento.Attributes["InterfazSalida"].Value.ToString() : "",
                            elemento.Attributes["ComponentePrincipal"].Value != null || elemento.Attributes["ComponentePrincipal"].Value.ToString() != "" ? elemento.Attributes["ComponentePrincipal"].Value.ToString() : "",
                            elemento.Attributes["Dependencias"].Value != null || elemento.Attributes["Dependencias"].Value.ToString() != "" ? elemento.Attributes["Dependencias"].Value.ToString() : "",
                            elemento.Attributes["BasedeDatos"].Value != null || elemento.Attributes["BasedeDatos"].Value.ToString() != "" ? elemento.Attributes["BasedeDatos"].Value.ToString() : "",
                            elemento.Attributes["IpBasedeDatos"].Value != null || elemento.Attributes["IpBasedeDatos"].Value.ToString() != "" ? elemento.Attributes["IpBasedeDatos"].Value.ToString() : "",
                            elemento.Attributes["HostnameBasedeDatos"].Value != null || elemento.Attributes["HostnameBasedeDatos"].Value.ToString() != "" ? elemento.Attributes["HostnameBasedeDatos"].Value.ToString() : "",
                            elemento.Attributes["Horario"].Value != null || elemento.Attributes["Horario"].Value.ToString() != "" ? elemento.Attributes["Horario"].Value.ToString() : "",
                            elemento.Attributes["Pais"].Value != null || elemento.Attributes["Pais"].Value.ToString() != "" ? elemento.Attributes["Pais"].Value.ToString() : "",
                            elemento.Attributes["EstatusAplicacion"].Value != null || elemento.Attributes["EstatusAplicacion"].Value.ToString() != "" ? elemento.Attributes["EstatusAplicacion"].Value.ToString() : "",
                            elemento.Attributes["EstatusControl"].Value != null || elemento.Attributes["EstatusControl"].Value.ToString() != "" ? elemento.Attributes["EstatusControl"].Value.ToString() : "",
                            elemento.Attributes["NombreEntrevistadoNegocio"].Value != null || elemento.Attributes["NombreEntrevistadoNegocio"].Value.ToString() != "" ? elemento.Attributes["NombreEntrevistadoNegocio"].Value.ToString() : "",
                            elemento.Attributes["Gerencia"].Value != null || elemento.Attributes["Gerencia"].Value.ToString() != "" ? elemento.Attributes["Gerencia"].Value.ToString() : "",
                            elemento.Attributes["Area"].Value != null || elemento.Attributes["Area"].Value.ToString() != "" ? elemento.Attributes["Area"].Value.ToString() : "",
                            elemento.Attributes["SubArea"].Value != null || elemento.Attributes["SubArea"].Value.ToString() != "" ? elemento.Attributes["SubArea"].Value.ToString() : "",
                            elemento.Attributes["PuestoEntrevistadoNegocio"].Value != null || elemento.Attributes["PuestoEntrevistadoNegocio"].Value.ToString() != "" ? elemento.Attributes["PuestoEntrevistadoNegocio"].Value.ToString() : "",
                            elemento.Attributes["ExpertoNegocio"].Value != null || elemento.Attributes["ExpertoNegocio"].Value.ToString() != "" ? elemento.Attributes["ExpertoNegocio"].Value.ToString() : "",
                            elemento.Attributes["InsumosEntrada"].Value != null || elemento.Attributes["InsumosEntrada"].Value.ToString() != "" ? elemento.Attributes["InsumosEntrada"].Value.ToString() : "",
                            elemento.Attributes["CualesInsumosEntrada"].Value != null || elemento.Attributes["CualesInsumosEntrada"].Value.ToString() != "" ? elemento.Attributes["CualesInsumosEntrada"].Value.ToString() : "",
                            elemento.Attributes["InsumosSalida"].Value != null || elemento.Attributes["InsumosSalida"].Value.ToString() != "" ? elemento.Attributes["InsumosSalida"].Value.ToString() : "",
                            elemento.Attributes["CualesInsumosSalida"].Value != null || elemento.Attributes["CualesInsumosSalida"].Value.ToString() != "" ? elemento.Attributes["CualesInsumosSalida"].Value.ToString() : "",
                            elemento.Attributes["ProcesoNormativo"].Value != null || elemento.Attributes["ProcesoNormativo"].Value.ToString() != "" ? elemento.Attributes["ProcesoNormativo"].Value.ToString() : "",
                            elemento.Attributes["ManualUsuario"].Value != null || elemento.Attributes["ManualUsuario"].Value.ToString() != "" ? elemento.Attributes["ManualUsuario"].Value.ToString() : "",
                            elemento.Attributes["DocumentoRequerimiento"].Value != null || elemento.Attributes["DocumentoRequerimiento"].Value.ToString() != "" ? elemento.Attributes["DocumentoRequerimiento"].Value.ToString() : "",
                            elemento.Attributes["ManualInstalacion"].Value != null || elemento.Attributes["ManualInstalacion"].Value.ToString() != "" ? elemento.Attributes["ManualInstalacion"].Value.ToString() : "",
                            elemento.Attributes["DocumentoSoporte"].Value != null || elemento.Attributes["DocumentoSoporte"].Value.ToString() != "" ? elemento.Attributes["DocumentoSoporte"].Value.ToString() : "",
                            elemento.Attributes["DiseñoTecnico"].Value != null || elemento.Attributes["DiseñoTecnico"].Value.ToString() != "" ? elemento.Attributes["DiseñoTecnico"].Value.ToString() : "",
                            elemento.Attributes["DiseñoFuncional"].Value != null || elemento.Attributes["DiseñoFuncional"].Value.ToString() != "" ? elemento.Attributes["DiseñoFuncional"].Value.ToString() : "",
                            elemento.Attributes["MatrizCasosUso"].Value != null || elemento.Attributes["MatrizCasosUso"].Value.ToString() != "" ? elemento.Attributes["MatrizCasosUso"].Value.ToString() : "",
                            elemento.Attributes["CasosPrueba"].Value != null || elemento.Attributes["CasosPrueba"].Value.ToString() != "" ? elemento.Attributes["CasosPrueba"].Value.ToString() : "",
                            elemento.Attributes["ErroresConocidos"].Value != null || elemento.Attributes["ErroresConocidos"].Value.ToString() != "" ? elemento.Attributes["ErroresConocidos"].Value.ToString() : "",
                            elemento.Attributes["NombreEntrevistadoTecnico"].Value != null || elemento.Attributes["NombreEntrevistadoTecnico"].Value.ToString() != "" ? elemento.Attributes["NombreEntrevistadoTecnico"].Value.ToString() : "",
                            elemento.Attributes["ExpertoAplicacionIT"].Value != null || elemento.Attributes["ExpertoAplicacionIT"].Value.ToString() != "" ? elemento.Attributes["ExpertoAplicacionIT"].Value.ToString() : "",
                            elemento.Attributes["SoftwareTerceros"].Value != null || elemento.Attributes["SoftwareTerceros"].Value.ToString() != "" ? elemento.Attributes["SoftwareTerceros"].Value.ToString() : "",
                            elemento.Attributes["FrameworksNegocio"].Value != null || elemento.Attributes["FrameworksNegocio"].Value.ToString() != "" ? elemento.Attributes["FrameworksNegocio"].Value.ToString() : "",
                            elemento.Attributes["VersionFrameworkNegocio"].Value != null || elemento.Attributes["VersionFrameworkNegocio"].Value.ToString() != "" ? elemento.Attributes["VersionFrameworkNegocio"].Value.ToString() : "",
                            elemento.Attributes["MotorBasedeDatos"].Value != null || elemento.Attributes["MotorBasedeDatos"].Value.ToString() != "" ? elemento.Attributes["MotorBasedeDatos"].Value.ToString() : "",
                            elemento.Attributes["VersionMotorBasedeDatos"].Value != null || elemento.Attributes["VersionMotorBasedeDatos"].Value.ToString() != "" ? elemento.Attributes["VersionMotorBasedeDatos"].Value.ToString() : "",
                            elemento.Attributes["EsquemaInstanciaBasedeDatos"].Value != null || elemento.Attributes["EsquemaInstanciaBasedeDatos"].Value.ToString() != "" ? elemento.Attributes["EsquemaInstanciaBasedeDatos"].Value.ToString() : "",
                            elemento.Attributes["ProcesosBatch"].Value != null || elemento.Attributes["ProcesosBatch"].Value.ToString() != "" ? elemento.Attributes["ProcesosBatch"].Value.ToString() : "",
                            elemento.Attributes["CualesProcesosBatch"].Value != null || elemento.Attributes["CualesProcesosBatch"].Value.ToString() != "" ? elemento.Attributes["CualesProcesosBatch"].Value.ToString() : "",
                            elemento.Attributes["AutomaticoManual"].Value != null || elemento.Attributes["AutomaticoManual"].Value.ToString() != "" ? elemento.Attributes["AutomaticoManual"].Value.ToString() : "",
                            elemento.Attributes["HorarioEjecucionBatch"].Value != null || elemento.Attributes["HorarioEjecucionBatch"].Value.ToString() != "" ? elemento.Attributes["HorarioEjecucionBatch"].Value.ToString() : "",
                            elemento.Attributes["ServidorDesarrollo"].Value != null || elemento.Attributes["ServidorDesarrollo"].Value.ToString() != "" ? elemento.Attributes["ServidorDesarrollo"].Value.ToString() : "",
                            elemento.Attributes["IpServidorDesarrollo"].Value != null || elemento.Attributes["IpServidorDesarrollo"].Value.ToString() != "" ? elemento.Attributes["IpServidorDesarrollo"].Value.ToString() : "",
                            elemento.Attributes["HostNameServidorDesarrollo"].Value != null || elemento.Attributes["HostNameServidorDesarrollo"].Value.ToString() != "" ? elemento.Attributes["HostNameServidorDesarrollo"].Value.ToString() : "",
                            elemento.Attributes["ServidorQA"].Value != null || elemento.Attributes["ServidorQA"].Value.ToString() != "" ? elemento.Attributes["ServidorQA"].Value.ToString() : "",
                            elemento.Attributes["IpServidorQA"].Value != null || elemento.Attributes["IpServidorQA"].Value.ToString() != "" ? elemento.Attributes["IpServidorQA"].Value.ToString() : "",
                            elemento.Attributes["HostnameServidorQA"].Value != null || elemento.Attributes["HostnameServidorQA"].Value.ToString() != "" ? elemento.Attributes["HostnameServidorQA"].Value.ToString() : ""
                            ));
                    }
                }
            }
            catch (Exception _e)
            {
                throw _e;
            }
            
        }
    }
}