﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Analizador_BP.Datos;
using System.Xml;
using Analizador_BP.Enums;

namespace Analizador_BP.Models
{
    public class UsuarioModel
    {
      
        int idUsuario;
        string nombre;
        string usuario;
        string password;
        string estatus;
        public DatosUsuario datos = new DatosUsuario();

        public int IdUsuario
        {
            get { return idUsuario; }
            set { idUsuario = value; }
        }

        [Required(ErrorMessage = "Nombre requerido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        [Display (Name="Nombre")]
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        [Required(ErrorMessage = "Usuario requerido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        [Display(Name= "Usuario")]       
        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        [Required (ErrorMessage = "Password requerido")]
        [StringLength(10, ErrorMessage = "Máximo 10 caracteres")]
        [Display (Name= "Password" )]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string Estatus
        {
            get { return estatus; }
            set { estatus = value; }
        }

        private XmlDocument detalleXML;
        public XmlDocument DetalleXML
        {
            get { return detalleXML; }
            set { detalleXML = value; }
        }

        public UsuarioModel()
        {
            if (datos.UsuariosConsulta((int)Enumeraciones_Usuarios.Consulta))
            {
                detalleXML = datos.ResultadoXML;
            }
        }
    }
}