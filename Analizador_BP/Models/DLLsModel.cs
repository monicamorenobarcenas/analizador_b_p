﻿using Analizador_BP.com.Datos;
using Analizador_BP.com.Utilerias;
using Analizador_BP.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Analizador_BP.Models
{
    public class DLLsModel
    {
        DatosDLL datosDDL = new DatosDLL();
        
        private List<ElementosDDL.EleDDL> listDDL = new List<ElementosDDL.EleDDL>();
        private List<ElementosDDL.EleDLLEliAgr> eleDLLEliAgr = new List<ElementosDDL.EleDLLEliAgr>();
        public List<ElementosDDL.EleDLLEliAgr> EleDLLEliAgr
        {
            get { return eleDLLEliAgr; }
            set { eleDLLEliAgr = value; }
        }

        private XmlDocument detalleXML;
        public XmlDocument DetalleXML
        {
            get { return detalleXML; }
            set { detalleXML = value; }
        }
        public DLLsModel()
        {
        }

        /// <summary>
        /// Realiza la consulta de todas las DDL's
        /// </summary>
        /// <param name="UsuarioID">es el de sesión</param>
        public DLLsModel(int UsuarioID)
        {
            if (datosDDL.DLLsConsulta((int)Enumeraciones_DLLs.Consulta, UsuarioID))
            {
                detalleXML = datosDDL.ResultadoXML;
            }
        }

        /// <summary>
        /// ELimina una DLL
        /// </summary>
        /// <param name="UsuarioID">Es el de sesion</param>
        /// <param name="Id_DLL">ID de la DLL a eliminar</param>
        public DLLsModel(int UsuarioID, int Id_DLL, string Nombre)
        {
            if (datosDDL.DLLELiminar((int)Enumeraciones_DLLs.Eliminar, UsuarioID, Id_DLL, Nombre))
            {

                XmlNode xmlNode = datosDDL.ResultadoXML.DocumentElement.SelectSingleNode("EliminarDLLs");
                foreach (XmlNode elemento in xmlNode.SelectNodes("row"))
                {
                    eleDLLEliAgr.Add(new ElementosDDL.EleDLLEliAgr(int.Parse(elemento.Attributes["Resultado"].Value.ToString())));
                }
            }
        }

        /// <summary>
        /// Agregar una DLL
        /// </summary>
        /// <param name="UsuarioID"></param>
        /// <param name="Nombre">completo de la dll</param>
        /// <param name="Ruta">Donde se almacenará</param>
        /// <param name="Tecnologia">a la que pertenece</param>
        public DLLsModel(int UsuarioID,string Nombre, string Ruta, string Tecnologia, string Version, string Descripcion_Cambios, string Nombre_Ejecucion)
        {
            if (datosDDL.DLLAgregar((int)Enumeraciones_DLLs.Agregar, UsuarioID, Nombre, Ruta, Tecnologia, Version, Descripcion_Cambios, Nombre_Ejecucion))
            {
                XmlNode xmlNode = datosDDL.ResultadoXML.DocumentElement.SelectSingleNode("AgregarDLLs");
                foreach (XmlNode elemento in xmlNode.SelectNodes("row"))
                {
                    eleDLLEliAgr.Add(new ElementosDDL.EleDLLEliAgr(int.Parse(elemento.Attributes["Resultado"].Value.ToString())));
                }
            }
        }


    }
}

