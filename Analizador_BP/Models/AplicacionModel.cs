﻿using Analizador_BP.com.Datos;
using Analizador_BP.Enums;
using Analizador_BP.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Analizador_BP.Models
{
    public class AplicacionModel : DatosAplicacion
    {
        int idAplicacion;
        string nombreCorto;
        string nombre;
        string tecnologia;
        string ruta;
        string framework;
        string tipoAplicacion;
        string versionLenguaje;
        string estatus;

        int idTecnologia;
        int idTipoAplicacion;
        int idCriticidad;

        public List<ListasDesplegables> tecnologiaLista = new List<ListasDesplegables>();
        public List<ListasDesplegables> tipoAplicacionLista = new List<ListasDesplegables>();
        public List<ListasDesplegables> criticidadLista = new List<ListasDesplegables>(); 

        List<SelectListItem> tecnologiaItems = new List<SelectListItem>();

        public string tecnologias = "Tecnologias";
        public string tipoApli = "TipoAplicacion";
        public string criticidades = "Criticidades";
        public XmlDocument tecnologiaXML;
        public XmlDocument criticidadesXML;
        public XmlDocument tipoAplicacionXML;
        public DatosAplicacion datos = new DatosAplicacion();

        [Required(ErrorMessage = "Nombre Corto requerido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        [Display(Name = "Nombre Corto")]
        public string NombreCorto {
            get { return nombreCorto; }
            set { nombreCorto = value; }
        }

        [Required(ErrorMessage = "Nombre requerido")]
        [StringLength(150, ErrorMessage = "Máximo 150 caracteres")]
        [Display(Name = "Nombre")]
        public string Nombre 
        { 
            get { return nombre; } 
            set { nombre = value; } 
        }
           
        public string Tecnologia 
        {
            get { return tecnologia; }
            set { tecnologia = value; }
        }

        [Required(ErrorMessage = "Ruta requerido")]
        [StringLength(1000, ErrorMessage = "Máximo 1000 caracteres")]
        [Display(Name = "Ruta")]
        public string Ruta 
        { 
            get { return ruta; } 
            set { ruta = value; }
        }

        [Required(ErrorMessage = "Framework requerido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        [Display(Name = "Framework")]
        public string Framework 
        { 
            get { return framework; } 
            set { framework = value; } 
        }
      

        public int IdAplicacion
        {
            get { return idAplicacion; }
            set { idAplicacion = value; }
        }
        
        [Required(ErrorMessage = "Seleccione una Tecnología")]
        [Display(Name = "Tecnología")]
        public int IdTecnologia
        {
            get { return idTecnologia; }
            set { idTecnologia = value; }
        }
       
        [Required(ErrorMessage = "Seleccione un Tipo de Aplicación")]
        [Display(Name = "Tipo de Aplicación")]
        public int IdTipoAplicacion
        {
            get { return idTipoAplicacion; }
            set { idTipoAplicacion = value; }
        }

        public string Estatus
        {
            get { return estatus; }
            set { estatus = value; }
        }

        public string TipoAplicacion 
        { 
            get { return tipoAplicacion; } 
            set { tipoAplicacion = value; } 
        }

        [Required(ErrorMessage = "Versión de Lenguaje requerido")]
        [StringLength(50, ErrorMessage = "Máximo 50 caracteres")]
        [Display(Name = "Versión Lenguaje")]
        public string VersionLenguaje 
        { 
            get { return versionLenguaje; } 
            set { versionLenguaje = value; } 
        }

        [Required(ErrorMessage = "Seleccione una Criticidad")]
        [Display(Name = "Criticidad")]
        public int IdCriticidad
        {
            get { return idCriticidad; }
            set { idCriticidad = value; }
        }
        public List<ListasDesplegables> TecnologiaLista
        {
            get { return tecnologiaLista; }
            set { tecnologiaLista = value; }
        }
               
        public List<ListasDesplegables> TipoAplicacionLista
        {
            get { return tipoAplicacionLista; }
            set { tipoAplicacionLista = value; }
        }

        public List<ListasDesplegables> CriticidadLista
        {
            get { return criticidadLista; }
            set { criticidadLista = value; }
        }
        public XmlDocument TecnologiaXML
        {
            get { return tecnologiaXML; }
            set { tecnologiaXML = value; }
        }
        

        public XmlDocument CriticidadesXML
        {
            get { return criticidadesXML; }
            set { criticidadesXML = value; }
        }
        public XmlDocument TipoAplicacionXML
        {
            get { return tipoAplicacionXML; }
            set { tipoAplicacionXML = value; }
        }

        private XmlDocument detalleXML;
        public XmlDocument DetalleXML
        {
            get { return detalleXML; }
            set { detalleXML = value; }
        }

        public AplicacionModel()
        {
            if (datos.AplicacionConsulta((int)Enumeraciones_Aplicaciones.AplicacionesConsulta))
            {
                detalleXML = datos.ResultadosXML;
            }

            if (consultaCatalogos((int)Enumeraciones_Aplicaciones.ConsultaTecnologia, tecnologias))
            {
                tecnologiaXML = ResultadosXML;
                XmlNode xmlNode2 = tecnologiaXML.DocumentElement.SelectSingleNode(tecnologias);
               
                foreach (XmlNode elemento in xmlNode2.SelectNodes("row"))
                {
                    tecnologiaLista.Add(new ListasDesplegables(
                        int.Parse(elemento.Attributes["Id_Tecnologia"].Value.ToString()),
                        elemento.Attributes["Nombre"].Value.ToString()));
                }             
            }
            if (consultaCatalogos((int)Enumeraciones_Aplicaciones.ConsultaTipoAplicacion, tipoApli))
            {
                tipoAplicacionXML = ResultadosXML;
                XmlNode xmlNode2 = tipoAplicacionXML.DocumentElement.SelectSingleNode(tipoApli);
                foreach (XmlNode elemento in xmlNode2.SelectNodes("row"))
                {                    
                    tipoAplicacionLista.Add(new ListasDesplegables(
                        int.Parse(elemento.Attributes["Id_TipoAplicacion"].Value.ToString()),
                        elemento.Attributes["Nombre"].Value.ToString()));
                }
            }
            if (consultaCatalogos((int)Enumeraciones_Aplicaciones.ConsultaCriticidades, criticidades))
            {
                criticidadesXML = ResultadosXML;
                XmlNode xmlNode2 = criticidadesXML.DocumentElement.SelectSingleNode(criticidades);
                foreach (XmlNode elemento in xmlNode2.SelectNodes("row"))
                {
                    criticidadLista.Add(new ListasDesplegables(
                        int.Parse(elemento.Attributes["Id_Criticidad"].Value.ToString()),
                        elemento.Attributes["Nombre"].Value.ToString()));
                }
            }
        }
    }
}