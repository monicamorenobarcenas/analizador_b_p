﻿using Analizador_BP.com.Utilerias;
using Analizador_BP.Datos;
using Analizador_BP.Enums;
using Analizador_BP.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml;

namespace Analizador_BP.Models
{
    public class LogsModel : DatosLogs
    {
        public XmlDocument detalleXML;
        private XmlDocument accionXML;
        private XmlDocument usuariosXML;
        public int tipo;
        public int idUsuario;
        public String accion;
        public String usuario;
        public String fechaInicio;
        public String fechaFin;
        public List<ListasDesplegables> accionLista = new List<ListasDesplegables>();
        public List<ListasDesplegables> usuariosLista = new List<ListasDesplegables>();
        public XmlDocument DetalleXML
        {
            get { return detalleXML; }
            set { detalleXML = value; }
        }

        public XmlDocument AccionXML
        {
            get { return accionXML; }
            set { accionXML = value; }
        }

        public XmlDocument UsuariosXML
        {
            get { return usuariosXML; }
            set { usuariosXML = value; }
        }


        public int Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public int IdUsuario
        {
            get { return idUsuario; }
            set { idUsuario = value; }
        }

        public String Accion
        {
            get { return accion; }
            set { accion = value; }
        }

        public String Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        public String FechaInicio
        {
            get { return fechaInicio; }
            set { fechaInicio = value; }
        }

        public String FechaFin
        {
            get { return fechaFin; }
            set { fechaFin = value; }
        }

        public List<ListasDesplegables> AccionLista
        {
            get { return accionLista; }
            set { accionLista = value; }
        }

        public List<ListasDesplegables> UsuariosLista
        {
            get { return usuariosLista; }
            set { usuariosLista = value; }
        }


        public LogsModel()
        {
            if (ObjetosLogsConsulta((int)Enumeraciones_Logs.ListadoLogs, idUsuario, accion, fechaInicio, fechaFin))
            {
                detalleXML = ResultadosXML;
            }

            if (ObjetosLogsConsulta((int)Enumeraciones_Logs.ListadoAcciones, idUsuario, accion, fechaInicio, fechaFin))
            {
                accionXML = ResultadosXML;
                XmlNode xmlNode2 = accionXML.DocumentElement.SelectSingleNode("Logs");
                int count = 0;
                foreach (XmlNode elemento in xmlNode2.SelectNodes("row"))
                {
                    count++;
                    accionLista.Add(new ListasDesplegables(
                        count,
                        elemento.Attributes["accion"].Value.ToString()));
                }
            }

            if (ObjetosLogsConsulta((int)Enumeraciones_Logs.ListadoUsuarios, idUsuario, accion, fechaInicio, fechaFin))
            {
                usuariosXML = ResultadosXML;
                XmlNode xmlNode2 = usuariosXML.DocumentElement.SelectSingleNode("Logs");

                foreach (XmlNode elemento in xmlNode2.SelectNodes("row"))
                {
                    usuariosLista.Add(new ListasDesplegables(
                        Int32.Parse(elemento.Attributes["Id_Usuario"].Value.ToString()),
                        elemento.Attributes["Usuario"].Value.ToString()));
                }
            }

        }
    }
}