﻿using Analizador_BP.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace Analizador_BP.Datos
{
    public class DatosLogs : GestionBD
    {
        private string SPLOGS = "Sp_Logs";
        private XmlDocument resultadosXML;
        public XmlDocument ResultadosXML
        {
            get { return resultadosXML; }
        }

        public bool ObjetosLogsConsulta(int Tipo, int idUsuario,
            string accion, string fechaInicio, string fechaFin)
        {
            bool respuesta = false;
            string usu = idUsuario.ToString();
            if (usu.Equals("0"))
            {
                usu = null;
            }
            try
            {
                PreparaStoredProcedure(SPLOGS);
                CargaParametro("tipo", SqlDbType.Int, 8, ParameterDirection.Input, Tipo);
                CargaParametro("id_usuario", SqlDbType.Int, 8, ParameterDirection.Input, usu);
                CargaParametro("accion", SqlDbType.VarChar, 20, ParameterDirection.Input, accion);
                CargaParametro("fecha_inicio", SqlDbType.VarChar, 20, ParameterDirection.Input, fechaInicio);
                CargaParametro("fecha_fin", SqlDbType.VarChar, 20, ParameterDirection.Input, fechaFin);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.HasRows)
                {
                    if (Lector.Read())
                    {
                        resultadosXML = new XmlDocument();
                        string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                        resultadosXML.LoadXml(Document);
                        XmlNode xmlNode = resultadosXML.DocumentElement.SelectSingleNode("Logs");
                        respuesta = xmlNode.HasChildNodes;
                    }
                }
                Lector.Close();
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString());
            }
            return respuesta;
        }
    }
}