﻿using Analizador_BP.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace Analizador_BP.com.Datos
{
    public class DatosAplicacion : GestionBD
    {

        private string SPAPLICACION = "sp_Aplicaciones";
        private XmlDocument resultadosXML;

        public XmlDocument ResultadosXML
        {
            get { return resultadosXML; }
        }

        public bool consultaCatalogos(int tipo, string catalogo)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(SPAPLICACION);
                CargaParametro("tipo", SqlDbType.Int, 8, ParameterDirection.Input, tipo);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.HasRows)
                {
                    if (Lector.Read())
                    {
                        resultadosXML = new XmlDocument();
                        string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                        resultadosXML.LoadXml(Document);
                        XmlNode xmlNode = resultadosXML.DocumentElement.SelectSingleNode(catalogo);
                        respuesta = xmlNode.HasChildNodes;
                    }
                }
                Lector.Close();
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.Write("BaseDeDatos.CerrarConexion " + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool AplicacionConsulta(int tipo)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(SPAPLICACION);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, tipo);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadosXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadosXML.LoadXml(Document);
                    XmlNode xmlNode = resultadosXML.DocumentElement.SelectSingleNode("Aplicaciones");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool AplicacionAgregar(int tipo, string nombreCorto, string nombre, string ruta, int idTecnologia, string framework,int idTipoAplicacion,string versionLenguaje,int IdUsuarioSe, int IdCriticidad)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(SPAPLICACION);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, tipo);
                CargaParametro("@nombreCorto", SqlDbType.VarChar, 50, ParameterDirection.Input, nombreCorto);
                CargaParametro("@nombre", SqlDbType.VarChar, 150, ParameterDirection.Input, nombre);
                CargaParametro("@Ruta", SqlDbType.VarChar, 1000, ParameterDirection.Input, ruta);
                CargaParametro("@idTecnologia", SqlDbType.Int, 8, ParameterDirection.Input, idTecnologia);
                CargaParametro("@framework", SqlDbType.VarChar, 50, ParameterDirection.Input, framework);
                CargaParametro("@idTipoAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, idTipoAplicacion);
                CargaParametro("@versionLenguaje", SqlDbType.VarChar, 50, ParameterDirection.Input, versionLenguaje);
                CargaParametro("@id_Usuario", SqlDbType.Int, 8, ParameterDirection.Input, IdUsuarioSe);
                CargaParametro("@IdCriticidad", SqlDbType.Int, 8, ParameterDirection.Input, IdCriticidad);
                EjecutaStoredProcedureNonQuery();
                CerrarConexion();
                respuesta = true;
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool AplicacionModificarBorradoLogico(int tipo, int idAplicacion, string nombreCorto, string nombre, string ruta, int idTecnologia, string framework, int idTipoAplicacion, string versionLenguaje, int IdUsuarioSe , int IdCriticidad)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(SPAPLICACION);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, tipo);
                CargaParametro("@Id_Aplicacion", SqlDbType.Int, 8, ParameterDirection.Input, idAplicacion);
                CargaParametro("@nombreCorto", SqlDbType.VarChar, 50, ParameterDirection.Input, nombreCorto);
                CargaParametro("@nombre", SqlDbType.VarChar, 150, ParameterDirection.Input, nombre);
                CargaParametro("@Ruta", SqlDbType.VarChar, 1000, ParameterDirection.Input, ruta);
                CargaParametro("@idTecnologia", SqlDbType.Int, 8, ParameterDirection.Input, idTecnologia);
                CargaParametro("@framework", SqlDbType.VarChar, 50, ParameterDirection.Input, framework);
                CargaParametro("@idTipoAplicacion", SqlDbType.Int, 8, ParameterDirection.Input, idTipoAplicacion);
                CargaParametro("@versionLenguaje", SqlDbType.VarChar, 50, ParameterDirection.Input, versionLenguaje);
                CargaParametro("@id_Usuario", SqlDbType.Int, 8, ParameterDirection.Input, IdUsuarioSe);
                CargaParametro("@IdCriticidad", SqlDbType.Int, 8, ParameterDirection.Input, IdCriticidad);
                EjecutaStoredProcedureNonQuery();
                CerrarConexion();
                respuesta = true;
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }
    }
}