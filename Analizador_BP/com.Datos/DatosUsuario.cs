﻿using Analizador_BP.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace Analizador_BP.Datos
{
    public class DatosUsuario : GestionBD
    {
        private string obtenUsuarios = "Sp_Usuarios";
        private XmlDocument resultadoXML;
        public XmlDocument ResultadoXML
        {
            get { return resultadoXML; }
        }

        public bool ValidacionLogin(int tipo, string usuario, string password)
        {
            bool resp = false;
            try
            {
                PreparaStoredProcedure(obtenUsuarios);
                CargaParametro("tipo", SqlDbType.Int, 8, ParameterDirection.Input, tipo);
                CargaParametro("usuario", SqlDbType.VarChar, 50, ParameterDirection.Input, usuario);
                CargaParametro("pass", SqlDbType.VarChar, 50, ParameterDirection.Input, password);               
                    SqlDataReader objrst = AlmacenarStoredProcedureDataReader();
                if (objrst.Read())
                {
                    resultadoXML = new XmlDocument();
                    resultadoXML.LoadXml("<xml>" + objrst[0].ToString() + "</xml>");
                    XmlNode select = resultadoXML.DocumentElement.SelectSingleNode("Usuarios");
                    resp = select.HasChildNodes;
                }
            }
            catch (Exception )
            {
                resp = false;
            }
            return resp;
        }

        public bool UsuariosConsulta(int tipo)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenUsuarios);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, tipo);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Usuarios");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }


        public bool UsuariosAgregar(int tipo, string nombre, string usuario, string password, int IdUsuarioSe)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenUsuarios);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, tipo);
                CargaParametro("@nombre", SqlDbType.VarChar, 150, ParameterDirection.Input, nombre);
                CargaParametro("@usuario", SqlDbType.VarChar, 50, ParameterDirection.Input, usuario);
                CargaParametro("@pass", SqlDbType.VarChar, 50, ParameterDirection.Input, password);
                CargaParametro("@IdUsuarioSes", SqlDbType.Int, 8, ParameterDirection.Input, IdUsuarioSe);
                EjecutaStoredProcedureNonQuery();
                CerrarConexion();
                respuesta = true;
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool UsuariosModificarBorradoLogico(int tipo, string nombre, string usuario, string password, int idUsuario, int IdUsuarioSe)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenUsuarios);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, tipo);
                CargaParametro("@nombre", SqlDbType.VarChar, 150, ParameterDirection.Input, nombre);
                CargaParametro("@usuario", SqlDbType.VarChar, 50, ParameterDirection.Input, usuario);
                CargaParametro("@pass", SqlDbType.VarChar, 50, ParameterDirection.Input, password);
                CargaParametro("@IdUsuario", SqlDbType.Int, 8, ParameterDirection.Input, idUsuario);
                CargaParametro("@IdUsuarioSes", SqlDbType.Int, 8, ParameterDirection.Input, IdUsuarioSe);
                EjecutaStoredProcedureNonQuery();
                CerrarConexion();
                respuesta = true;
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }
    }
}