﻿using Analizador_BP.com.Utilerias;
using Analizador_BP.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace Analizador_BP.com.Datos
{
    public class DatosDLL : GestionBD
    {
        const string sp_DLLs = "sp_DLLs";
        private XmlDocument resultadoXML;
        public XmlDocument ResultadoXML
        {
            get { return resultadoXML; }
        }

        public bool DLLsConsulta(int Tipo, int UsuarioID)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(sp_DLLs);
                CargaParametro("@Tipo", SqlDbType.Int, 8, ParameterDirection.Input, Tipo);
                CargaParametro("@IdUsuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("DDls");
                    respuesta = xmlNode.HasChildNodes;
                }
                Lector.Close();
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.Write("Excepcion: DatosDDL.DLLsConsulta " + Err.Message.ToString());
            }
            return respuesta;
        }
        public bool DLLELiminar(int Tipo, int UsuarioID, int Id_DLL, string Nombre)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(sp_DLLs);
                CargaParametro("@Tipo", SqlDbType.Int, 8, ParameterDirection.Input, Tipo);
                CargaParametro("@IdUsuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                CargaParametro("@Id_DLL", SqlDbType.Int, 8, ParameterDirection.Input, Id_DLL);
                CargaParametro("@Nombre", SqlDbType.VarChar, 150, ParameterDirection.Input, Nombre);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("EliminarDLLs");
                    respuesta = xmlNode.HasChildNodes;
                }
                Lector.Close();
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosDDL.DLLELiminar " + Err.Message.ToString());
            }
            return respuesta;
        }
        public bool DLLAgregar(int Tipo, int UsuarioID, string Nombre, string Ruta, string Tecnologia, string Version, string Descripcion_Cambios, string Nombre_Ejecucion)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(sp_DLLs);
                CargaParametro("@Tipo", SqlDbType.Int, 8, ParameterDirection.Input, Tipo);
                CargaParametro("@IdUsuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                CargaParametro("@Nombre", SqlDbType.VarChar, 150, ParameterDirection.Input, Nombre);
                CargaParametro("@Ruta", SqlDbType.VarChar, 1000, ParameterDirection.Input, Ruta);
                CargaParametro("@Tecnologia", SqlDbType.VarChar, 50, ParameterDirection.Input, Tecnologia);
                CargaParametro("@Nombre_Ejecucion", SqlDbType.VarChar, 50, ParameterDirection.Input, Tecnologia);
                CargaParametro("@Version", SqlDbType.VarChar, 50, ParameterDirection.Input, Version);
                CargaParametro("@Descripcion_Cambios", SqlDbType.VarChar, 500, ParameterDirection.Input, Descripcion_Cambios);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("AgregarDLLs");
                    respuesta = xmlNode.HasChildNodes;
                }
                Lector.Close();
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosDDL.DLLAgregar " + Err.Message.ToString());
            }
            return respuesta;
        }
    }
}

