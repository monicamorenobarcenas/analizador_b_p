﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using Analizador_BP.com.Utilerias;
using Analizador_BP.Models;
using Analizador_BP.Utility;

namespace Analizador_BP.Datos
{
    public class DatosProcesos : GestionBD
    {
        const string obtenProcesos = "sp_Procesos";
        const string obtenTecnologias = "sp_CatTecnologias";
        const string obtenTipoAnalizador = "sp_CatTipoAnalizador";
        const string obtenAplicaciones = "sp_Aplicaciones";
        const string obtenerDLL = "sp_DLLs";
        const string InventarioExcel = "sp_InventarioExcel";

        private XmlDocument resultadoXML;   
        public XmlDocument ResultadoXML
        {
            get { return resultadoXML; }
        }

        public bool ProcesosConsulta(int UsuarioID)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenProcesos);
                CargaParametro("@id_Usuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Procesos");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }
        public bool AplicacionesConsulta(int Tipo, int UsuarioID)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenAplicaciones);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, Tipo);
                CargaParametro("@id_Usuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Aplicaciones");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }
        public bool AplicacionesConsultaFechaProceso(int Tipo, string Fecha_Creacion, string Fecha_Fin)
        {
            bool respuesta = false;
            try
            {                
                PreparaStoredProcedure(obtenAplicaciones);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, Tipo);
                if (Fecha_Creacion != "1900-01-01")
                {
                    CargaParametro("@Fecha_Creacion", SqlDbType.VarChar, 20, ParameterDirection.Input, Fecha_Creacion);
                }                
                if (Fecha_Fin != "1900-01-01")
                {
                    CargaParametro("@Fecha_Fin", SqlDbType.VarChar, 20, ParameterDirection.Input, Fecha_Fin);
                }
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Aplicaciones");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }
        public bool AplicacionesActualiza(int Tipo, int UsuarioID, int Id_Aplicacion, string NombreCarpeta)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenAplicaciones);
                CargaParametro("@tipo", SqlDbType.Int, 8, ParameterDirection.Input, Tipo);
                CargaParametro("@id_Usuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                CargaParametro("@Id_Aplicacion", SqlDbType.Int, 8, ParameterDirection.Input, Id_Aplicacion);
                CargaParametro("@Ruta", SqlDbType.VarChar, 1000, ParameterDirection.Input, NombreCarpeta);
                EjecutaStoredProcedureNonQuery();
                CerrarConexion();
                respuesta = true;
            }
            catch (Exception Err)
            {
                respuesta = false;
                Console.WriteLine("Excepcion: DatosProcesos.AplicacionesActualiza" + Err.Message.ToString());
            }
            return respuesta;
        }
        public bool TipoAnalizadorConsulta(int UsuarioID)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenTipoAnalizador);
                CargaParametro("@id_Usuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Analizador");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosObjetosCM.ObjetosCMConsulta" + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool DLLsConsulta(int Tipo, int UsuarioID, int Id_Aplicacion)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(obtenerDLL);
                CargaParametro("@Tipo", SqlDbType.Int, 8, ParameterDirection.Input, Tipo);
                CargaParametro("@IdUsuario", SqlDbType.Int, 8, ParameterDirection.Input, UsuarioID);
                CargaParametro("@Id_Aplicacion", SqlDbType.Int, 8, ParameterDirection.Input, Id_Aplicacion);
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("DDls");
                    respuesta = xmlNode.HasChildNodes;
                }
                Lector.Close();
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.Write("Excepcion: DatosDDL.DLLsConsulta " + Err.Message.ToString());
            }
            return respuesta;
        }

        public bool ConsultaExcel(int Id_Aplicacion, string ids_Aplicaciones)
        {
            bool respuesta = false;
            try
            {
                PreparaStoredProcedure(InventarioExcel);
                if (Id_Aplicacion!=0)
                {
                    CargaParametro("@Id_Aplicacion", SqlDbType.Int, 8, ParameterDirection.Input, Id_Aplicacion);
                }
                if (ids_Aplicaciones != "")
                {
                    CargaParametro("@ids_Aplicaciones", SqlDbType.VarChar, int.Parse(ids_Aplicaciones.Length.ToString()) , ParameterDirection.Input, ids_Aplicaciones);
                }
                
                SqlDataReader Lector = AlmacenarStoredProcedureDataReader();
                if (Lector.Read())
                {
                    resultadoXML = new XmlDocument();
                    string Document = "<xml>" + Lector[0].ToString() + "</xml>";
                    resultadoXML.LoadXml(Document);
                    XmlNode xmlNode = resultadoXML.DocumentElement.SelectSingleNode("Inventario");
                    respuesta = xmlNode.HasChildNodes;
                }
                CerrarConexion();
            }
            catch (Exception Err)
            {
                Console.WriteLine("Excepcion: DatosProcesos.ConsultaPDF" + Err.Message.ToString());
            }
            return respuesta;
        }
    }
}