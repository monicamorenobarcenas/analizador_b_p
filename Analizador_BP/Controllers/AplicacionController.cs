﻿using Analizador_BP.com.Datos;
using Analizador_BP.Datos;
using Analizador_BP.Enums;
using Analizador_BP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Analizador_BP.Controllers
{
    public class AplicacionController : Controller
    {
        public DatosAplicacion datos = new DatosAplicacion();
        public AplicacionModel model = new AplicacionModel();
        // GET: Aplicacion

        public ActionResult Index()
        {
            if (Session["idUsuario"] != null)
            {
                var tablero = new AplicacionModel();
                return View(tablero);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public ActionResult AgregarPantalla()
        {
            if (Session["idUsuario"] != null)
            {
                var tableroAplicacion = new AplicacionModel();
                return View("AgregarAplicacion", tableroAplicacion);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public ActionResult AgregaAplicacion(AplicacionModel aplicacionModel)
        {
            if (Session["idUsuario"] != null)
            {
                if (!ModelState.IsValid)
                {
                    return View("AgregarAplicacion", aplicacionModel);
                }

                if (datos.AplicacionAgregar((int)Enumeraciones_Aplicaciones.AplicacionesAgregar,
                    aplicacionModel.NombreCorto, aplicacionModel.Nombre, aplicacionModel.Ruta, aplicacionModel.IdTecnologia,
                    aplicacionModel.Framework, aplicacionModel.IdTipoAplicacion, aplicacionModel.VersionLenguaje, int.Parse(Session["idUsuario"].ToString()), aplicacionModel.IdCriticidad))
                {
                    if (datos.AplicacionConsulta((int)Enumeraciones_Aplicaciones.AplicacionesConsulta))
                    {
                        aplicacionModel.DetalleXML = datos.ResultadosXML;
                    }
                    ViewBag.message = "Exitoso";
                    return View("Index", aplicacionModel);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public ActionResult ModificarAplicacion(AplicacionModel aplicacionModel)
        {
            if (Session["idUsuario"] != null)
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.message = "Campos Requeridos";
                    return View("Index", aplicacionModel);
                }

                if (datos.AplicacionModificarBorradoLogico((int)Enumeraciones_Aplicaciones.AplicacionesModificar,
                    aplicacionModel.IdAplicacion, aplicacionModel.NombreCorto, aplicacionModel.Nombre, 
                    aplicacionModel.Ruta, aplicacionModel.IdTecnologia, aplicacionModel.Framework, aplicacionModel.IdTipoAplicacion, aplicacionModel.VersionLenguaje, 
                    int.Parse(Session["idUsuario"].ToString()), aplicacionModel.IdCriticidad))
                {
                    if (datos.AplicacionConsulta((int)Enumeraciones_Aplicaciones.AplicacionesConsulta))
                    {
                        aplicacionModel.DetalleXML = datos.ResultadosXML;
                    }
                    ViewBag.message = "Exitoso";
                    return View("Index", aplicacionModel);            
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public ActionResult EliminarAplicacion(int idAplicacion, string nombre)
        {
            if (Session["idUsuario"] != null)
            {
                if (datos.AplicacionModificarBorradoLogico((int)Enumeraciones_Aplicaciones.AplicacionesEliminar, 
                    idAplicacion,null,nombre, null, 0,null,0,null, int.Parse(Session["idUsuario"].ToString()),0))
                {
                    if (datos.AplicacionConsulta((int)Enumeraciones_Aplicaciones.AplicacionesConsulta))
                    {
                        model.DetalleXML = datos.ResultadosXML;
                    }
                    return PartialView("Detalle", model);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
    }    
}