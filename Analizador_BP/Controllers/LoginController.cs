﻿using Analizador_BP.com.Utilerias;
using Analizador_BP.Datos;
using Analizador_BP.Enums;
using Analizador_BP.Models;
using Analizador_BP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Analizador_BP.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login

        DatosUsuario datosUsuario = new DatosUsuario();
        public ActionResult Index()
        {
            Session["idUsuario"] = null;
            Session["sesionNombre"] = null;
            Session["sesionUsuario"] = null;            
            return View();
        }

        public ActionResult validacionDatosLogin(LoginModel loginModel)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", loginModel);
            }
            if (datosUsuario.ValidacionLogin((int)Enumeraciones_Usuarios.ValidaLogin, loginModel.Usuario, loginModel.Contraseña))
            {
                XmlNode xmlNode = datosUsuario.ResultadoXML.DocumentElement.SelectSingleNode("Usuarios");
                XmlNode infoUsuario = xmlNode.ChildNodes[0];     
                if (int.Parse(infoUsuario.Attributes["Id_Usuario"].Value) > 0)
                {
                    Session["sesionNombre"] = infoUsuario.Attributes["nombre"].Value.ToString();
                    Session["sesionUsuario"] = infoUsuario.Attributes["usuario"].Value.ToString();
                    Session["idUsuario"] = infoUsuario.Attributes["Id_Usuario"].Value;
                    return RedirectToAction("Index", "Analizador");
                }
            }

            ViewBag.message = "Usuario y/o Contraseña incorrectos, favor de volver a intentarlo";
             return View("Index", loginModel);
        }
    }
}