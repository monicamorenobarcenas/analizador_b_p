﻿
using Analizador_BP.Models;
using Analizador_BP.Datos;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;
using System.Diagnostics;
using Analizador_BP.Enums;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;
//using analizador_csharp;

namespace Analizador_BP.Controllers
{
    public class AnalizadorController : Controller
    {
        String rutaDLL = "";
        String nombreDLL = "";
        String nombreEjecucion = "";
        public DatosProcesos Datos = new DatosProcesos();
        AnalizadorModel AnaliMod = new AnalizadorModel();

        // GET: Analizador
        public ActionResult Index()
        {
            if (Session["idUsuario"] != null)
            {
                var tableroGeneralModel = new AnalizadorModel(int.Parse(Session["idUsuario"].ToString()));
                return View(tableroGeneralModel);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public string FileDialogo()
        {
            string selectedPath = "";
            Thread t = new Thread((ThreadStart)(() =>
            {
                FolderBrowserDialog saveFileDialog1 = new FolderBrowserDialog();

                var boolAccion = saveFileDialog1.ShowDialog();
                if (boolAccion == DialogResult.OK)
                {
                    selectedPath = saveFileDialog1.SelectedPath;
                }
                if (boolAccion == DialogResult.Cancel)
                {
                    selectedPath = "Seleccionar un proyecto";
                }
            }));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            return selectedPath;
        }
        public bool PreparaAnalizador(int[] Parameters, int Id_Aplicacion, string ruta, string NombreArchivoProyecto)
        {
            bool respuesta = false;
            var AnalizadorModel = new AnalizadorModel((int)Enumeraciones_DLLs.ConsultaUna, int.Parse(Session["idUsuario"].ToString()), Id_Aplicacion);

            if (AnalizadorModel != null)
            {
                rutaDLL = AnalizadorModel.ListDll[0].Ruta.ToString();
                nombreDLL = AnalizadorModel.ListDll[0].Nombre.ToString();
                nombreEjecucion = AnalizadorModel.ListDll[0].Nombre_Ejecucion.ToString();

                Assembly SampleAssembly;
                SampleAssembly = Assembly.LoadFrom(rutaDLL + "\\" + nombreDLL);

                Type dllType = SampleAssembly.GetType("analizador_" + nombreEjecucion + ".Inicio"); //analizador_csharp
                ConstructorInfo dllConstructor = dllType.GetConstructor(Type.EmptyTypes);
                object dllClassObject = dllConstructor.Invoke(new object[] { });

                MethodInfo dllMethod = dllType.GetMethod("IniciarAnalisis");
                object dllValue = dllMethod.Invoke(dllClassObject, new object[] { int.Parse(Session["idUsuario"].ToString()), Id_Aplicacion, Parameters, ruta, ConfigurationManager.ConnectionStrings["ConexionSQL"].ConnectionString.ToString() });

                //Inicio inicio = new Inicio();
                //object dllValue = inicio.IniciarAnalisis(int.Parse(Session["idUsuario"].ToString()), Id_Aplicacion, Parameters, ruta, ConfigurationManager.ConnectionStrings["ConexionSQL"].ConnectionString.ToString());

                Datos.AplicacionesActualiza((int)Enumeraciones_Aplicaciones.Actualiza, int.Parse(Session["idUsuario"].ToString()), Id_Aplicacion, NombreArchivoProyecto);
                respuesta = true;
            }
            else
            {
                respuesta = false;
            }
            return respuesta;
        }

        public ActionResult ConsultaAplicacionesFechaProceso(string Fecha_Creacion, string Fecha_Fin, int Tipo)
        {
            
            if (Session["idUsuario"] != null)
            {
                if (Tipo == 0)
                {
                    AnaliMod.DetalleXMLAplicaciones = TabAplicaciones(Fecha_Creacion, Fecha_Fin,Tipo);
                    return PartialView("Aplicaciones", AnaliMod);
                }
                else
                {
                    AnaliMod.DetalleXMLAplicaciones = TabAplicaciones(Fecha_Creacion, Fecha_Fin, Tipo);
                    return PartialView("Aplicaciones", AnaliMod);
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public XmlDocument TabAplicaciones(string Fecha_Creacion, string Fecha_Fin, int Tipo)
        {
            XmlDocument detalleXML = new XmlDocument();
            if (Tipo == 0)
            {
                if (Datos.AplicacionesConsultaFechaProceso((int)Enumeraciones_Aplicaciones.ConsultaPorFecha, Fecha_Creacion, Fecha_Fin))
                {
                    detalleXML = Datos.ResultadoXML;
                }
            }
            else
            {
                if(Datos.AplicacionesConsulta(Tipo, int.Parse(Session["idUsuario"].ToString())))
                {
                    detalleXML = Datos.ResultadoXML;
                }
            }
            
            return detalleXML;
        }
        #region Excel
        public int CrearExcelTodos(int Id_Aplicacion, string ids_Aplicaciones)
        {
            int respuesta = 0;
            if (Session["idUsuario"] != null)
            {
                
                var grupoDepModel = new AnalizadorModel(Id_Aplicacion, ids_Aplicaciones, int.Parse(Session["idUsuario"].ToString()));
                if (grupoDepModel.ListExcel.Count > 0)
                {
                    #region Variables
                    string fileNameoriginal = "BP2_DueDiligence_InventarioAplicaciones_ 09Dic2019.xlsx";
                    string fileNameCopia = DateTime.Now.Day + Convert.ToString(DateTime.Now.ToString("MMMM")).ToUpper().Substring(0, 3) + DateTime.Now.Year;
                    fileNameCopia = "BP2_DueDiligence_InventarioAplicaciones_" + fileNameCopia;
                    string ext = ".xlsx";
                    string originalPath = @"C:\Users\monica.moreno\Desktop\Código fuente_ Analizador\Analizador_BP\Analizador_BP\Excel";
                    string copiaPath = @"C:\Users\monica.moreno\Downloads";
                    int countador = 0;
                    #endregion
                    try
                    {
                        #region Si existe el directorio
                        if (System.IO.Directory.Exists(copiaPath))
                        {
                            #region Copia de con base al nombre - comentado                  
                            string[] files = System.IO.Directory.GetFiles(copiaPath);
                            string NombreCompara = copiaPath + @"\" + fileNameCopia + ext;
                            foreach (var item in files)
                            {
                                var cadena = item;
                                var separador = new char[] { '(' }; // un espacio en blanco
                                var arregloDeSubCadenas = cadena.Split(separador);
                                if (arregloDeSubCadenas.Count() == 1)
                                {
                                    if (System.IO.File.Exists(arregloDeSubCadenas[0].ToString()))
                                    {
                                        if (arregloDeSubCadenas[0] == NombreCompara)
                                        {
                                            countador++;
                                        }
                                    }
                                }
                                else
                                {
                                    string www = arregloDeSubCadenas[0].ToString() + ".xlsx";
                                    if (System.IO.File.Exists(www))
                                    {
                                        if (www == NombreCompara)
                                        {
                                            countador++;
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region Creamos una copia del excel
                            string sourceFile = System.IO.Path.Combine(originalPath, fileNameoriginal);
                            string destFile = System.IO.Path.Combine(copiaPath, fileNameCopia + (countador == 0 ? "" : "(" + countador + ")") + ext);
                            System.IO.Directory.CreateDirectory(copiaPath);
                            System.IO.File.Copy(sourceFile, destFile, true);
                            Console.WriteLine("Copia creada..............OK");
                            #endregion

                            #region Se llena excel
                            Application ExApp = new Application();
                            Workbook oWBook;
                            //Worksheet oSheet;
                            string ruta = @"C:\Users\monica.moreno\Downloads\" + fileNameCopia + (countador == 0 ? "" : "(" + countador + ")") + ext;

                            oWBook = ExApp.Workbooks.Open(ruta, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                            #region Enumeracion de Hojas
                            Worksheet ws1 = oWBook.Worksheets[1];//Responsabilidad
                            Worksheet ws2 = oWBook.Worksheets[2];//Glosario
                            Worksheet ws3 = oWBook.Worksheets[3];//Inventario de Aplicaciones
                            Worksheet ws4 = oWBook.Worksheets[4];//Información de Sanidad
                            #endregion

                            #region Escribir dentro de excel
                            int count = 4;
                            foreach (var item in grupoDepModel.ListExcel)
                            {
                                ws3.Rows[count].WrapText = true;
                                //oSheet.Cells[4, A] 
                                ws3.Cells[count, 1] = item.Aplicacion;
                                ws3.Cells[count, 1].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 5] = item.ClaveSonarQube; // Sonnar
                                ws3.Cells[count, 5].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 6] = item.NombreCarpeta; // Se cambia por nombre de la carpeta
                                ws3.Cells[count, 6].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 31] = item.TipoAplicacion;
                                ws3.Cells[count, 31].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 32] = item.URL;
                                ws3.Cells[count, 32].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 34] = item.Tecnologia;
                                ws3.Cells[count, 34].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 35] = item.Lenguaje;
                                ws3.Cells[count, 35].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 36] = item.Ambiente;
                                ws3.Cells[count, 36].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 37] = item.IPServidor;
                                ws3.Cells[count, 37].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 38] = item.S0;
                                ws3.Cells[count, 38].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 39] = item.Version_SistemaOperativo;
                                ws3.Cells[count, 39].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 40] = item.Hostname;
                                ws3.Cells[count, 40].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 41] = item.Workstation;
                                ws3.Cells[count, 41].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 42] = item.Middleware;
                                ws3.Cells[count, 42].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 43] = item.VersionadorCodigo;
                                ws3.Cells[count, 43].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 44] = item.IPRepositorio;
                                ws3.Cells[count, 44].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 45] = item.CodigoFuente;
                                ws3.Cells[count, 45].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 46] = item.Documentacion;
                                ws3.Cells[count, 46].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 47] = item.ArchivoConexion;
                                ws3.Cells[count, 47].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 48] = item.InterfazEntrada;
                                ws3.Cells[count, 48].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 49] = item.InterfazEntrada;
                                ws3.Cells[count, 49].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 50] = item.ComponentePrincipal;
                                ws3.Cells[count, 50].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 51] = item.Dependencias;
                                ws3.Cells[count, 51].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 52] = item.BasedeDatos;
                                ws3.Cells[count, 52].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 53] = item.IpBasedeDatos;
                                ws3.Cells[count, 53].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 54] = item.HostnameBasedeDatos;
                                ws3.Cells[count, 54].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 55] = item.Horario;
                                ws3.Cells[count, 55].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 56] = item.Pais;
                                ws3.Cells[count, 56].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 57] = item.EstatusAplicacion;
                                ws3.Cells[count, 57].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 58] = item.EstatusControl;
                                ws3.Cells[count, 58].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 59] = item.NombreEntrevistadoNegocio;
                                ws3.Cells[count, 59].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 60] = item.Gerencia;
                                ws3.Cells[count, 60].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 61] = item.Area;
                                ws3.Cells[count, 61].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 62] = item.SubArea;
                                ws3.Cells[count, 62].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 63] = item.PuestoEntrevistadoNegocio;
                                ws3.Cells[count, 63].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 64] = item.ExpertoNegocio;
                                ws3.Cells[count, 64].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 65] = item.InsumosEntrada;
                                ws3.Cells[count, 65].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 66] = item.CualesInsumosEntrada;
                                ws3.Cells[count, 66].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 67] = item.InsumosSalida;
                                ws3.Cells[count, 67].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 68] = item.CualesInsumosSalida;
                                ws3.Cells[count, 68].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 69] = item.ProcesoNormativo;
                                ws3.Cells[count, 69].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 70] = item.ManualUsuario;
                                ws3.Cells[count, 70].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 71] = item.DocumentoRequerimiento;
                                ws3.Cells[count, 71].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 72] = item.ManualInstalacion;
                                ws3.Cells[count, 72].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 73] = item.DocumentoSoporte;
                                ws3.Cells[count, 73].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 74] = item.DiseñoTecnico;
                                ws3.Cells[count, 74].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 75] = item.DiseñoFuncional;
                                ws3.Cells[count, 75].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 76] = item.MatrizCasosUso;
                                ws3.Cells[count, 76].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 77] = item.CasosPrueba;
                                ws3.Cells[count, 77].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 78] = item.ErroresConocidos;
                                ws3.Cells[count, 78].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 79] = item.NombreEntrevistadoTecnico;
                                ws3.Cells[count, 79].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 80] = item.ExpertoAplicacionIT;
                                ws3.Cells[count, 80].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 81] = item.SoftwareTerceros;
                                ws3.Cells[count, 81].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 83] = item.Framework;
                                ws3.Cells[count, 83].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                                ws3.Cells[count, 83].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 84] = item.MotorBasedeDatos;
                                ws3.Cells[count, 84].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 85] = item.VersionMotorBasedeDatos;
                                ws3.Cells[count, 85].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 86] = item.EsquemaInstanciaBasedeDatos;
                                ws3.Cells[count, 86].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 87] = item.ProcesosBatch;
                                ws3.Cells[count, 87].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 88] = item.CualesProcesosBatch;
                                ws3.Cells[count, 88].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 89] = item.AutomaticoManual;
                                ws3.Cells[count, 89].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 90] = item.HorarioEjecucionBatch;
                                ws3.Cells[count, 90].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 91] = item.ServidorDesarrollo;
                                ws3.Cells[count, 91].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 92] = item.IpServidorDesarrollo;
                                ws3.Cells[count, 92].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 93] = item.HostNameServidorDesarrollo;
                                ws3.Cells[count, 93].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 94] = item.ServidorQA;
                                ws3.Cells[count, 94].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 95] = item.IpServidorQA;
                                ws3.Cells[count, 95].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                ws3.Cells[count, 96] = item.HostnameServidorQA;
                                ws3.Cells[count, 96].VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                count++;
                            }

                            #endregion

                            ExApp.Visible = false;
                            ExApp.UserControl = true;
                            oWBook.Save();
                            oWBook.Close();
                            ExApp.Quit();
                            ExApp = null;
                            #region Abrir excel
                            string comando = "/c Start " + ruta;
                            ProcessStartInfo ps = new ProcessStartInfo("cmd", comando);
                            ps.UseShellExecute = false;
                            ps.RedirectStandardOutput = true;
                            Process p = Process.Start(ps);
                            p.WaitForExit();
                            #endregion
                            respuesta = 1;
                            return respuesta;
                            #endregion
                        }
                        else
                        {
                            respuesta = 0;
                            Console.WriteLine("No existe el directorio");
                        }
                        #endregion
                    }
                    catch (Exception _e)
                    {
                        respuesta = 0;
                        return respuesta;

                    }
                    
                }
                else
                {
                    respuesta = 2;
                    return respuesta;
                }
                return respuesta;
            }
            else
            {
                 RedirectToAction("Index", "Login");
            }
            return respuesta;
        }
        #endregion

        #region Actualizar Detalle

        public ActionResult ActualizaTablaAnalizador()
        {
            if (Session["idUsuario"] != null)
            {
                AnaliMod.DetalleXMLProcesos = TabAnalizado(int.Parse(Session["idUsuario"].ToString()));
                return PartialView("Detalle", AnaliMod);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public XmlDocument TabAnalizado(int UsuarioID)
        {
            XmlDocument detalleXML = new XmlDocument();
            if (Datos.ProcesosConsulta(UsuarioID))
            {
                detalleXML = Datos.ResultadoXML;
            }
            return detalleXML;
        }

        public JsonResult ConsultaInfoPluggin(int Id_Aplicacion)
        {
            var cc = Json("", JsonRequestBehavior.AllowGet);
            var grupoDepModel = new AnalizadorModel((int)Enumeraciones_DLLs.ConsultaUna, int.Parse(Session["idUsuario"].ToString()), Id_Aplicacion);
            cc = Json(grupoDepModel.ListDll, JsonRequestBehavior.AllowGet);
                    return cc;
           
        }

        #endregion
    }
}