﻿using Analizador_BP.com.Datos;
using Analizador_BP.com.Utilerias;
using Analizador_BP.Enums;
using Analizador_BP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;
using System.Xml;

namespace Analizador_BP.Controllers
{
    public class DLLsController : Controller
    {
        #region Variables Globales
        DLLsModel dLLsMod = new DLLsModel();
        DatosDLL datosDLL = new DatosDLL();
        private List<ElementosDDL.EleDDL> listDDL = new List<ElementosDDL.EleDDL>();
        private List<ElementosDDL.EleDLLEliAgr> eleDLLEliAgr = new List<ElementosDDL.EleDLLEliAgr>();
        #endregion

        #region Index
        public ActionResult Index()
        {
            if (Session["idUsuario"] != null)
            {
                var DllsModel = new DLLsModel(int.Parse(Session["idUsuario"].ToString()));
                return View(DllsModel);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        #endregion

        #region Actualizar Detalle

        public ActionResult ActualizaTablaDLLs()
        {
            if (Session["idUsuario"] != null)
            {
                dLLsMod.DetalleXML = TabDLL(int.Parse(Session["idUsuario"].ToString()));
                return PartialView("Detalle", dLLsMod);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public XmlDocument TabDLL(int UsuarioID)
        {
            XmlDocument detalleXML = new XmlDocument();
            if (datosDLL.DLLsConsulta((int)Enumeraciones_DLLs.Consulta, UsuarioID))
            {
                detalleXML = datosDLL.ResultadoXML;
            }
            return detalleXML;
        }

        #endregion

        #region Eliminar

        public JsonResult Eliminar_DLL(int Id_DLL, string Nombre)
        {
            var cc = Json("", JsonRequestBehavior.AllowGet);
            if (Session["idUsuario"] != null)
            {
                var grupoDepModel = new DLLsModel(int.Parse(Session["idUsuario"].ToString()), Id_DLL, Nombre);
                cc = Json(grupoDepModel.EleDLLEliAgr, JsonRequestBehavior.AllowGet);
                return cc;
            }

            else
            {
                RedirectToAction("Index", "Login");
                cc = Json("", JsonRequestBehavior.AllowGet);
            }
            return cc;

        }

        
        #endregion

        #region Agregar DLL

        public JsonResult Agregar_DLL(string Nombre, string Tecnologia, string RutaOrigen,  string Version, string Descripcion_Cambios, string Nombre_Ejecucion)
        {
            var cc = Json("", JsonRequestBehavior.AllowGet);
            if (Session["idUsuario"] != null)
            {
                string RutaDestino = @"C:\Users\monica.moreno\Desktop\Plugins_Analizador";
                if (GuardarDLLRuta(RutaOrigen, Nombre, RutaDestino))
                {                   
                    var grupoDepModel = new DLLsModel(int.Parse(Session["idUsuario"].ToString()), Nombre, RutaDestino, Tecnologia, Version, Descripcion_Cambios, Nombre_Ejecucion); 
                    cc = Json(grupoDepModel.EleDLLEliAgr, JsonRequestBehavior.AllowGet);
                    return cc;
                }
            }
            else
            {
                RedirectToAction("Index", "Login");
                cc = Json("", JsonRequestBehavior.AllowGet);
            }
            return cc;
        }

       

        public bool GuardarDLLRuta(string RutaOrigen, string Nombre, string RutaDestino)
        {
            bool respuesta = false;
            try
            {
                string destinoFile = System.IO.Path.Combine(RutaDestino, Nombre);
                System.IO.File.Copy(RutaOrigen, destinoFile, true);
                respuesta = true;
            }
            catch (Exception _e)
            {
                respuesta = false;
            }
            return respuesta;
        }


        public string FileDialogo()
        {
            string selectedPath = "";
            Thread t = new Thread((ThreadStart)(() =>
            {
                OpenFileDialog saveFileDialog1 = new OpenFileDialog();
                saveFileDialog1.Filter = "DLL Files (*.dll)|*.dll";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;
                var boolAccion = saveFileDialog1.ShowDialog();
                if (boolAccion == DialogResult.OK)
                {
                    selectedPath = saveFileDialog1.FileName;
                }
                if (boolAccion == DialogResult.Cancel)
                {
                    selectedPath = "Seleccionar archivo (.dll)";
                }

            }));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            return selectedPath;

        }
        #endregion
    }
}