﻿using Analizador_BP.com.Utilerias;
using Analizador_BP.Datos;
using Analizador_BP.Enums;
using Analizador_BP.Models;
using Analizador_BP.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Analizador_BP.Controllers
{
    public class LogsController : Controller
    {
        DatosLogs datosLogs = new DatosLogs();
        LogsModel logsModel = new LogsModel();
        // GET: Logs
        public ActionResult Index()
        {
            var tableroGeneralModel = new LogsModel();
            return View("Logs", tableroGeneralModel);
        }
        public ActionResult actualizarTablaLogs(String usu,
            String acc, String fechaIni, String fechaFin)
        {
            logsModel.DetalleXML = xmlTabLogs((int)Enumeraciones_Logs.ListadoLogs, usu, acc, fechaIni, fechaFin);
            return PartialView("Detalle", logsModel);
        }

        public XmlDocument xmlTabLogs(int tipo, string usu, string acc, string fechaIni, string fechaFin)
        {
            XmlDocument xmlLogs = new XmlDocument();
            int idUsu = 0;
            string act = null;

            if (String.IsNullOrEmpty(usu))
            {
                idUsu = 0;
            }
            else
            {
                idUsu = Int32.Parse(usu);
            }

            if (!String.IsNullOrEmpty(acc))
            {
                act = accion(acc);
            }

            if (datosLogs.ObjetosLogsConsulta(tipo, idUsu, act, fechaIni, fechaFin))
            {
                xmlLogs = datosLogs.ResultadosXML;
            }
            return xmlLogs;
        }

        public string accion(string acc)
        {
            string act = "";
            ListasDesplegables listaDesplegables = new ListasDesplegables();
            foreach (ListasDesplegables lista in logsModel.accionLista)
            {
                if (lista.Indice == Int32.Parse(acc))
                {
                    act = lista.Texto;
                    break;
                }
            }
            return act;
        }
    }
}