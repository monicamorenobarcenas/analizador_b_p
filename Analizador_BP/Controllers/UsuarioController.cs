﻿using Analizador_BP.Datos;
using Analizador_BP.Enums;
using Analizador_BP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Analizador_BP.Controllers
{
    public class UsuarioController : Controller
    {
 

        // GET: Usuario
        public DatosUsuario datos = new DatosUsuario();
        UsuarioModel model = new UsuarioModel();

        public ActionResult Index()
        {
            if (Session["idUsuario"] != null)
            {
                var tablero = new UsuarioModel();
                return View(tablero);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            
        }

        public ActionResult AgregarPantalla()
        {
            if (Session["idUsuario"] != null)
            {
                return View("AgregarUsuario");
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            
        }

        public ActionResult AgregarUsuario(UsuarioModel usuarioModel)
        {
            if (Session["idUsuario"] != null)
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.message = "Campos Agregar";
                    return View("AgregarUsuario", usuarioModel);
                }

                if (datos.UsuariosAgregar((int)Enumeraciones_Usuarios.Insertar, usuarioModel.Nombre, usuarioModel.Usuario, usuarioModel.Password, int.Parse(Session["idUsuario"].ToString())))
                {
                    if (datos.UsuariosConsulta((int)Enumeraciones_Usuarios.Consulta))
                    {
                        usuarioModel.DetalleXML = datos.ResultadoXML;
                    }

                    ViewBag.message = "Exitoso";
                    return View("Index", usuarioModel);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            
        }

        public ActionResult ModificarUsuario(UsuarioModel usuarioModel)
        {
            if (Session["idUsuario"] != null)
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.message = "Campos Requeridos";
                    return View("Index", usuarioModel);
                }

                if (datos.UsuariosModificarBorradoLogico((int)Enumeraciones_Usuarios.Modifica, usuarioModel.Nombre, usuarioModel.Usuario, usuarioModel.Password, usuarioModel.IdUsuario, int.Parse(Session["idUsuario"].ToString())))
                {
                    if (datos.UsuariosConsulta((int)Enumeraciones_Usuarios.Consulta))
                    {
                        usuarioModel.DetalleXML = datos.ResultadoXML;
                    }
                    ViewBag.message = "Exitoso";
                    return View("Index", usuarioModel);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            
        }

        public ActionResult EliminarUsuario(int idUsuario, string nombre)
        {
            if (Session["idUsuario"] != null)
            {
                if (datos.UsuariosModificarBorradoLogico((int)Enumeraciones_Usuarios.Borrar, nombre, null, null, idUsuario, int.Parse(Session["idUsuario"].ToString())))
                {
                    if (datos.UsuariosConsulta((int)Enumeraciones_Usuarios.Consulta))
                    {
                        model.DetalleXML = datos.ResultadoXML;
                    }
                    return PartialView("Detalle", model);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            
        }
    }
}